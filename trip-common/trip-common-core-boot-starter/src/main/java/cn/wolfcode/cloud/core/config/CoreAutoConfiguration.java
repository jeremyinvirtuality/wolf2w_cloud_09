package cn.wolfcode.cloud.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "cn.wolfcode.cloud.core")
public class CoreAutoConfiguration {
}

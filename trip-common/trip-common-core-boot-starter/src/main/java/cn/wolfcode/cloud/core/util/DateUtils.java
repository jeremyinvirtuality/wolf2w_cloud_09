package cn.wolfcode.cloud.core.util;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;

abstract public class DateUtils {

    public static long getBetweenSecondsOfNowToLastSeconds() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime lastSeconds = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 23, 59, 59);
        return getBetweenSeconds(now, lastSeconds);
    }

    public static long getBetweenMinutes(Temporal start, Temporal end) {
        return getBetween(start, end, ChronoUnit.MINUTES);
    }

    public static long getBetweenSeconds(Temporal start, Temporal end) {
        return getBetween(start, end, ChronoUnit.SECONDS);
    }

    public static long getBetween(Temporal start, Temporal end, ChronoUnit unit) {
        return unit.between(start, end);
    }
}

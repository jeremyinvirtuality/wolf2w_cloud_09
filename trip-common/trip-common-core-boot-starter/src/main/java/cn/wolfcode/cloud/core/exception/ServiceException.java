package cn.wolfcode.cloud.core.exception;

import cn.wolfcode.cloud.core.util.R;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceException extends RuntimeException {

    private Integer code = R.DEFAULT_ERROR_CODE;

    public ServiceException() {
        super(R.DEFAULT_ERROR_MSG);
    }

    public ServiceException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}

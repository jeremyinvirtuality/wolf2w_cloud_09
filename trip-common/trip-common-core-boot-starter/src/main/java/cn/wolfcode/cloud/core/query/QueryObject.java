package cn.wolfcode.cloud.core.query;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryObject {

    private Integer current = 1;
    private Integer size = 10;
    private String keyword;
}

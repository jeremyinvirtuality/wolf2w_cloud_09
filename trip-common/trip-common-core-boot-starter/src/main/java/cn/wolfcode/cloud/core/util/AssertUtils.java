package cn.wolfcode.cloud.core.util;

import cn.wolfcode.cloud.core.exception.ServiceException;

import java.util.Objects;

abstract public class AssertUtils {

    public static final Integer BUSINESS_ERROR_CODE = 400;

    public static void isNull(Object obj, String msg) {
        if (obj != null) {
            throw new ServiceException(BUSINESS_ERROR_CODE, msg);
        }
    }

    /**
     * 断言 obj 对象不为 null，如果为 null 则抛出异常
     */
    public static void notNull(Object obj, String msg) {
        if (obj == null) {
            throw new ServiceException(BUSINESS_ERROR_CODE, msg);
        }
    }

    public static void notBlank(String str, String msg) {
        if (str == null || "".equals(str)) {
            throw new ServiceException(BUSINESS_ERROR_CODE, msg);
        }
    }

    public static void equals(Object obj1, Object obj2, String msg) {
        if (!Objects.equals(obj1, obj2)) {
            throw new ServiceException(BUSINESS_ERROR_CODE, msg);
        }
    }

    public static void equalsIgnoreCase(String str1, String str2, String msg) {
        if (!(str1 == null ? str2 == null : str1.equalsIgnoreCase(str2))) {
            throw new ServiceException(BUSINESS_ERROR_CODE, msg);
        }
    }
}

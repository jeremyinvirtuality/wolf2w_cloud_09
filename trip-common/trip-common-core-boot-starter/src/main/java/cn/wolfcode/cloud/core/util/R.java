package cn.wolfcode.cloud.core.util;

import com.alibaba.fastjson2.JSON;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class R<T> {

    public static final Integer DEFAULT_ERROR_CODE = 500;
    public static final String DEFAULT_ERROR_MSG = "服务繁忙";

    public static final Integer DEFAULT_SUCCESS_CODE = 200;
    public static final String DEFAULT_SUCCESS_MSG = "success";

    private Integer code;
    private String msg;
    private T data;

    public static <V> R<V> ok() {
        return ok(null);
    }

    public static <V> R<V> ok(V data) {
        return new R<>(DEFAULT_SUCCESS_CODE, DEFAULT_SUCCESS_MSG, data);
    }

    public static R<?> error(String msg) {
        return error(DEFAULT_ERROR_CODE, msg);
    }

    public static R<?> toAjax(boolean result) {
        return result ? ok() : error("操作失败");
    }

    public static R<?> error(Integer code, String msg) {
        return new R<>(code, msg, null);
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}

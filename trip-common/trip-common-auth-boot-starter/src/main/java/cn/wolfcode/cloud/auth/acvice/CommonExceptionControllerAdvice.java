package cn.wolfcode.cloud.auth.acvice;

import cn.wolfcode.cloud.core.exception.ServiceException;
import cn.wolfcode.cloud.core.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class CommonExceptionControllerAdvice {

    private static final String flag = "统一异常处理";

    @ExceptionHandler(Exception.class)
    public R<?> fullExceptionHandler(Exception e) {
        log.error("[{}] 出现系统异常：{}", flag, e.getMessage());
        log.error("异常栈信息", e);
        return R.error(e.getMessage());
    }

    @ExceptionHandler(ServiceException.class)
    public R<?> serviceExceptionHandler(ServiceException e) {
        log.warn("[{}] 业务异常：{}", flag, e.getMessage());
        return R.error(e.getCode(), e.getMessage());
    }
}

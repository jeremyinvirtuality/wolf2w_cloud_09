package cn.wolfcode.cloud.auth.interceptor;

import cn.wolfcode.cloud.auth.anno.RequiredLogin;
import cn.wolfcode.cloud.auth.config.JwtAuthProperties;
import cn.wolfcode.cloud.core.exception.ServiceException;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
@RequiredArgsConstructor
public class AuthenticationInterceptor implements HandlerInterceptor {

    private final JwtAuthProperties jwtAuthProperties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // handler 在 MVC 中可能是三种类型的对象
        // 1. HandlerMethod 就是贴了 @RequestMapping 的方法
        // 2. 静态资源处理器，当访问静态资源的时候用到
        // 3. 预请求处理器，当浏览器跨域时会发起 OPTIONS 类型的请求到后台询问是否允许跨域
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        HandlerMethod hm = (HandlerMethod) handler;
        // 判断方法或类上是否有 @RequiredLogin 注解，如果都没有，就直接放行
        RequiredLogin classAnno = hm.getBeanType().getAnnotation(RequiredLogin.class);
        if (hm.getMethodAnnotation(RequiredLogin.class) == null && classAnno == null) {
            return true;
        }

        // 从请求头中获取 token
        JwtAuthProperties.JwtConfig jwt = jwtAuthProperties.getJwt();
        String token = request.getHeader(jwt.getHeaderName());
        if (StringUtils.isEmpty(token)) {
            throw new ServiceException(401, "请登录后再访问");
        }

        try {
            Jwts.parser()
                    .setSigningKey(jwt.getPrivateKey())
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            log.warn("[登录拦截器] 解析 token 失败：{}", e.getMessage());
            // 如果解析出现异常，说明 token 无效（已过期/错误）
            throw new ServiceException(401, "请登录后再访问");
        }

        return true;
    }
}

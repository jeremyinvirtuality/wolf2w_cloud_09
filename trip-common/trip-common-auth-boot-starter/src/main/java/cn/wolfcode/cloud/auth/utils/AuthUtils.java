package cn.wolfcode.cloud.auth.utils;

import cn.wolfcode.cloud.auth.config.JwtAuthProperties;
import cn.wolfcode.cloud.auth.vo.LoginUser;
import cn.wolfcode.cloud.core.exception.ServiceException;
import cn.wolfcode.cloud.redis.key.RedisKeyEnum;
import cn.wolfcode.cloud.redis.service.RedisService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Slf4j
@Component
@RequiredArgsConstructor
public class AuthUtils {

    private final JwtAuthProperties jwtAuthProperties;
    private final RedisService redisService;

    public String getToken() {
        String headerName = jwtAuthProperties.getJwt().getHeaderName();
        return RequestUtil.getRequest().getHeader(headerName);
    }

    public LoginUser getLoginUser() {
        return getLoginUser(false);
    }

    public LoginUser getLoginUser(boolean requiredLogin) {
        // 1. 获取到 redis 对象
        // 2. 先获取 token
        String token = getToken();
        if (StringUtils.isEmpty(token) || "undefined".equals(token)) {
            if (requiredLogin) {
                throw new ServiceException(401, "请先登录");
            }
            return null;
        }
        try {
            // 3. 解析 token 得到 phone
            Claims claims = Jwts.parser()
                    .setSigningKey(jwtAuthProperties.getJwt().getPrivateKey())
                    .parseClaimsJws(token)
                    .getBody();
            // 3. 拼接 redis key，得到用户对象
            String phone = claims.getSubject();
            // 4. 返回对象
            return redisService.get(RedisKeyEnum.LOGIN_USER_KEY, phone);
        } catch (Exception e) {
            log.warn("[认证工具] 解析 token 获取登录对象失败", e);
            if (requiredLogin) {
                throw new ServiceException(401, "请先登录");
            }
        }
        return null;
    }
}

package cn.wolfcode.cloud.auth.config;

import cn.wolfcode.cloud.auth.interceptor.AuthenticationInterceptor;
import cn.wolfcode.cloud.auth.resolver.LoginUserArgumentResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@ComponentScan("cn.wolfcode.cloud.auth")
@RequiredArgsConstructor
@Configuration
@EnableConfigurationProperties(JwtAuthProperties.class)
public class TripWebMvcConfiguration implements WebMvcConfigurer {

    private final AuthenticationInterceptor authenticationInterceptor;
    private final JwtAuthProperties jwtAuthProperties;
    private final LoginUserArgumentResolver loginUserArgumentResolver;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(loginUserArgumentResolver);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authenticationInterceptor)
                .addPathPatterns(jwtAuthProperties.getPathPatterns())
                .excludePathPatterns(jwtAuthProperties.getExcludePathPatterns());
    }
}

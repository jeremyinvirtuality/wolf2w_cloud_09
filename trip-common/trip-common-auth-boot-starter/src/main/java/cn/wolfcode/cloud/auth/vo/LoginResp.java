package cn.wolfcode.cloud.auth.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginResp {

    private String accessToken;
    private String refreshToken;
    private LoginUser user;
}

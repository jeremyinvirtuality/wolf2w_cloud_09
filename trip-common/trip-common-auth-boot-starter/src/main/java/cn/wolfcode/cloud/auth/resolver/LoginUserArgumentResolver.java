package cn.wolfcode.cloud.auth.resolver;

import cn.wolfcode.cloud.auth.anno.RequestUser;
import cn.wolfcode.cloud.auth.utils.AuthUtils;
import cn.wolfcode.cloud.auth.vo.LoginUser;
import lombok.RequiredArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

@Component
@RequiredArgsConstructor
public class LoginUserArgumentResolver implements HandlerMethodArgumentResolver {

    private final AuthUtils authUtils;

    /**
     * 是否支持该参数
     *
     * @param parameter 所请求的方法上的每一个参数
     * @return 是否支持这个参数
     */
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        RequestUser anno = parameter.getParameterAnnotation(RequestUser.class);
        return anno != null && parameter.getParameterType().equals(LoginUser.class);
    }

    /**
     * 解析参数，并返回结果
     * 如果 supportsParameter 返回的结果为 false，这个方法就会被执行，并且将当前方法执行返回的结果赋值给当前参数
     */
    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        RequestUser anno = parameter.getParameterAnnotation(RequestUser.class);
        boolean required = false;
        if (anno != null) {
            required = anno.required();
        }
        return authUtils.getLoginUser(required);
    }
}

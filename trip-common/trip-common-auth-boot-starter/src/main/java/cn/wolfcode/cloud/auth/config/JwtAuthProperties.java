package cn.wolfcode.cloud.auth.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Getter
@Setter
@ConfigurationProperties("auth")
public class JwtAuthProperties {

    private List<String> pathPatterns;
    private List<String> excludePathPatterns;
    private JwtConfig jwt;

    @Getter
    @Setter
    public static class JwtConfig {
        private String privateKey;
        private String headerName;
        private String algo;
        private Long expiration;
    }
}

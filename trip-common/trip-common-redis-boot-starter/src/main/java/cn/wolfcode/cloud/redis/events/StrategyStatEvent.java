package cn.wolfcode.cloud.redis.events;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * 攻略统计事件对象
 */
@Getter
@Setter
public class StrategyStatEvent extends ApplicationEvent {

    private String hashKey;
    private Integer num;

    public StrategyStatEvent(Long strategyId, String hashKey, Integer num) {
        super(strategyId);
        this.hashKey = hashKey;
        this.num = num;
    }
}

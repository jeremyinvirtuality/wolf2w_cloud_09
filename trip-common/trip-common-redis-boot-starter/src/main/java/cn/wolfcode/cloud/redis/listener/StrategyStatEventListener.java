package cn.wolfcode.cloud.redis.listener;


import cn.wolfcode.cloud.redis.events.StrategyStatEvent;
import cn.wolfcode.cloud.redis.key.RedisKeyEnum;
import cn.wolfcode.cloud.redis.service.RedisService;
import com.alibaba.fastjson2.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class StrategyStatEventListener implements ApplicationListener<StrategyStatEvent> {

    private final RedisService redisService;

    @Override
    public void onApplicationEvent(StrategyStatEvent event) {
        log.info("[攻略统计] 收到攻略统计事件：{}", JSON.toJSONString(event));
        redisService.hincrement(RedisKeyEnum.STRATEGY_STATICS_HASH, event.getHashKey(),
                event.getNum(), event.getSource() + "");
    }
}

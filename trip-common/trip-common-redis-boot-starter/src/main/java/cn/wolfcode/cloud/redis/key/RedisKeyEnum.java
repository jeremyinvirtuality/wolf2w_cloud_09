package cn.wolfcode.cloud.redis.key;

import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.TimeUnit;

/**
 * 专门封装访问 Redis 的 key 信息
 */
@Getter
public enum RedisKeyEnum {

    STRATEGY_THUMB_USERS_HASH("articles:strategies:thumb:users"),
    STRATEGY_STATICS_DATA_INIT_FLAG("articles:strategies:stat:data:init:flag"),
    STRATEGY_STATICS_HASH("articles:strategies:statics"),
    STRATEGY_CONDITIONS_DESTINATIONS("articles:strategies:conditions:destinations"),
    STRATEGY_CONDITIONS_THEMES("articles:strategies:conditions:themes"),
    REGISTER_SMS_VERIFY_CODE("members:sms:regsteir:verfycode", 10L, TimeUnit.MINUTES),
    LOGIN_USER_KEY("members:login:users", 2L, TimeUnit.HOURS);

    /**
     * key 拼接的分隔符
     */
    public static final String SEPARATOR = ":";

    RedisKeyEnum(String prefix) {
        this(prefix, -1L, TimeUnit.SECONDS);
    }

    RedisKeyEnum(String prefix, Long timeout, TimeUnit unit) {
        this.prefix = prefix;
        this.timeout = timeout;
        this.unit = unit;
    }

    private final String prefix;
    @Setter
    private Long timeout;
    private final TimeUnit unit;

    /**
     * 拼接当前 key 的前缀与后缀
     *
     * @param suffix 后缀
     * @return 完整的 key
     */
    public String concat(String... suffix) {
        StringBuilder sb = new StringBuilder(100);
        sb.append(prefix);
        for (String s : suffix) {
            sb.append(SEPARATOR).append(s);
        }
        return sb.toString();
    }
}

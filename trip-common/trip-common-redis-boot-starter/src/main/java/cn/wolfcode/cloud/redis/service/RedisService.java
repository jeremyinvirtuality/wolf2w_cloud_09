package cn.wolfcode.cloud.redis.service;

import cn.wolfcode.cloud.redis.key.RedisKeyEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;

/**
 * 1. 基于 Redis 的五种数据结构定义方法命名的规范
 * 2. 方法名尽量靠近 Redis 原生命令，以及尽量保证可读性
 * <p>
 * STRING：所有方法没有特殊前缀
 * HASH：所有方法以 h 开头
 * LIST：所有方法以 l 开头
 * SET：所有方法以 s 开头
 * ZSET：所有方法以 z 开头
 */
@Service
@RequiredArgsConstructor
public class RedisService {

    private final RedisTemplate<String, Object> redisTemplate;

    public void set(RedisKeyEnum prefix, Object value, String... suffix) {
        if (prefix.getTimeout() > 0) {
            redisTemplate.opsForValue().set(prefix.concat(suffix), value, prefix.getTimeout(), prefix.getUnit());
        } else {
            redisTemplate.opsForValue().set(prefix.concat(suffix), value);
        }
    }

    public <T> T get(RedisKeyEnum prefix, String... suffix) {
        return (T) redisTemplate.opsForValue().get(prefix.concat(suffix));
    }

    public Boolean del(RedisKeyEnum prefix, String... suffix) {
        return redisTemplate.delete(prefix.concat(suffix));
    }

    public boolean hexists(RedisKeyEnum prefix, String field, String... suffix) {
        Boolean exists = redisTemplate.opsForHash().hasKey(prefix.concat(suffix), field);
        return Boolean.TRUE.equals(exists);
    }

    public void hputAll(RedisKeyEnum prefix, Map<String, Object> map, String... suffix) {
        redisTemplate.opsForHash().putAll(prefix.concat(suffix), map);
    }

    public Map<Object, Object> hgetAll(RedisKeyEnum prefix, String... suffix) {
        return redisTemplate.opsForHash().entries(prefix.concat(suffix));
    }

    public Long hincrement(RedisKeyEnum prefix, String field, int increment, String... suffix) {
        return redisTemplate.opsForHash().increment(prefix.concat(suffix), field, increment);
    }

    public void hdel(RedisKeyEnum prefix, Object[] fields, String... suffix) {
        redisTemplate.opsForHash().delete(prefix.concat(suffix), fields);
    }

    public boolean exists(RedisKeyEnum prefix, String... suffix) {
        return Boolean.TRUE.equals(redisTemplate.hasKey(prefix.concat(suffix)));
    }

    public boolean setnx(RedisKeyEnum prefix, Object value, String... suffix) {
        Boolean result = false;
        if (prefix.getTimeout() > 0) {
            result = redisTemplate.opsForValue().setIfAbsent(prefix.concat(suffix), value, prefix.getTimeout(), prefix.getUnit());
        } else {
            result = redisTemplate.opsForValue().setIfAbsent(prefix.concat(suffix), value);
        }
        return Boolean.TRUE.equals(result);
    }

    public Set<String> keys(String pattern) {
        return redisTemplate.keys(pattern);
    }

    public boolean hsetnx(RedisKeyEnum prefix, String field, Object value, String... suffix) {
        boolean absent = false;
        if (prefix.getTimeout() > 0) {
            absent = redisTemplate.opsForHash().putIfAbsent(prefix.concat(suffix), field, value);
            // 设置超时时间
            redisTemplate.expire(prefix.concat(suffix), prefix.getTimeout(), prefix.getUnit());
        } else {
            absent = redisTemplate.opsForHash().putIfAbsent(prefix.concat(suffix), field, value);
        }
        return absent;
    }
}

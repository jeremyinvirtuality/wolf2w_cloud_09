package cn.wolfcode.clcoud.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RegisterParam {

    private String nickname;
    private String phone;
    private String password;
    private String verifyCode;
}

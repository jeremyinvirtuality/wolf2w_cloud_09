
package cn.wolfcode.clcoud.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class UserInfoDTO implements Serializable {

    private Long id;
    private String city;  //所在城市
    private String info;  //个性签名
    private String nickname;  //昵称
    private String phone;  //手机
    private String email;  //邮箱
    private String password; //密码
    private Integer gender; //性别
    private Integer level;  //用户级别
    private String headImgUrl; //头像
    private Integer state; //状态
}


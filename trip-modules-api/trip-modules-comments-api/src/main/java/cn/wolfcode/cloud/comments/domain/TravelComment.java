package cn.wolfcode.cloud.comments.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 游记评论
 */
@Setter
@Getter
@TableName("travel_comments")
public class TravelComment implements Serializable {

    public static final int TYPE_ARTICLE = 0; //普通评论
    public static final int TYPE_REPLY = 1; //评论的评论

    @TableId(type = IdType.AUTO)
    private String id;  //id
    private Long travelId;  //游记id
    private String travelTitle; //游记标题
    private Long userId;    //用户id
    private String nickname; //用户名
    private String city;
    private Integer level;
    private String headImgUrl;   // 用户头像
    private Integer type; //评论类别
    private Date createTime; //创建时间
    private String content;  //评论内容
    private Long refCommentId;
    @TableField(exist = false)
    private TravelComment refComment;  //关联的评论
}
package cn.wolfcode.cloud.comments.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 攻略评论
 */
@Setter
@Getter
@TableName("strategy_comments")
@ToString
public class StrategyComment implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;
    private Long strategyId;  //攻略(明细)id
    private String strategyTitle; //攻略标题
    private Long userId;    //用户id
    private String nickname;  //用户名
    private String city;
    private Integer level;
    private String headImgUrl;     //头像
    private Date createTime;    //创建时间
    private String content;      //评论内容
    private Integer thumbupnum;     //点赞数

    @TableField(exist = false)
    private List<Long> thumbuplist = new ArrayList<>();
}
package cn.wolfcode.cloud.comments.query;

import cn.wolfcode.cloud.core.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentQuery extends QueryObject {

    private Long articleId;
}

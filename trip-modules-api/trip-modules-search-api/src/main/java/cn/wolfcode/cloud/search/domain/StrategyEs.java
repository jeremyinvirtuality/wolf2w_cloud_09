package cn.wolfcode.cloud.search.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;

import static cn.wolfcode.cloud.search.domain.StrategyEs.INDEX_NAME;

/**
 * 攻略搜索对象
 */
@Getter
@Setter
@Document(indexName = INDEX_NAME)
public class StrategyEs implements Serializable {

    public static final String INDEX_NAME = "strategies";

    //@Field 每个文档的字段配置（类型、是否分词、是否存储、分词器 ）
    @Id
    @Field(type = FieldType.Long)
    private Long id;  //攻略id
    @Field(analyzer = "ik_smart", searchAnalyzer = "ik_smart", type = FieldType.Text)
    private String title;  //攻略标题
    @Field(analyzer = "ik_smart", searchAnalyzer = "ik_smart", type = FieldType.Text)
    private String subTitle;  //攻略标题
    @Field(analyzer = "ik_smart", searchAnalyzer = "ik_smart", type = FieldType.Text)
    private String summary; //攻略简介
    private Long destId;  //关联的目的地
    private String destName;
    private Long themeId; //关联主题
    private String themeName;
    private Long catalogId;  //关联的分类
    private String catalogName;
    private String coverUrl;  //封面
    @Field(format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss", type = FieldType.Date)
    private String createTime;  //创建时间
    private Integer isabroad;  //是否是国外
    private Integer viewnum;  //点击数
    private Integer replynum;  //攻略评论数
    private Integer favornum; //收藏数
    private Integer sharenum; //分享数
    private Integer thumbsupnum; //点赞个数
    private Integer state;  //状态
}
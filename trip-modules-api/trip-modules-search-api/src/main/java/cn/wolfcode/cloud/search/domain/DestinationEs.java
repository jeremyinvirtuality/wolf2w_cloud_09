package cn.wolfcode.cloud.search.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 * 目的地搜索对象
 */
@Getter
@Setter
@Document(indexName = DestinationEs.INDEX_NAME)
public class DestinationEs implements Serializable {

    public static final String INDEX_NAME = "destinations";

    @Id
    //@Field 每个文档的字段配置（类型、是否分词、是否存储、分词器 ）
    @Field(type = FieldType.Long)
    private Long id;  //攻略id
    @Field(type = FieldType.Keyword)
    private String name;
    @Field(analyzer = "ik_smart", searchAnalyzer = "ik_smart", type = FieldType.Text)
    private String info;

    private String english;  //英文名
    private Long parentId; //上级目的地
    private String parentName;  //上级目的名
    private String coverUrl;
}
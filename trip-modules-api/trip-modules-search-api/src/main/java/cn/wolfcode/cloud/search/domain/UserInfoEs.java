package cn.wolfcode.cloud.search.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 * 用户搜索对象
 */
@Getter
@Setter
@Document(indexName = "userinfo")
public class UserInfoEs implements Serializable {

    public static final String INDEX_NAME = "userinfo";

    @Id
    //@Field 每个文档的字段配置（类型、是否分词、是否存储、分词器 ）
    @Field(type = FieldType.Long)
    private Long id;  //用户id
    @Field(type = FieldType.Keyword)
    private String city;
    @Field(analyzer = "ik_smart", searchAnalyzer = "ik_smart", type = FieldType.Text)
    private String info;
    private String nickname;  //昵称
    private String phone;  //手机
    private String email;  //邮箱
    private String password; //密码
    private Integer gender; //性别
    private Integer level;  //用户级别
    private String headImgUrl; //头像
    private Integer state; //状态
}
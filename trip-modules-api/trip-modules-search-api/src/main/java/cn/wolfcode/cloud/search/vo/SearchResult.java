package cn.wolfcode.cloud.search.vo;

import cn.wolfcode.cloud.search.domain.DestinationEs;
import cn.wolfcode.cloud.search.domain.StrategyEs;
import cn.wolfcode.cloud.search.domain.TravelEs;
import cn.wolfcode.cloud.search.domain.UserInfoEs;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.List;

@Getter
@Setter
public class SearchResult {

    private Long total = 0L;
    private List<DestinationEs> destinations = Collections.emptyList();
    private List<StrategyEs> strategies = Collections.emptyList();
    private List<TravelEs> travels = Collections.emptyList();
    private List<UserInfoEs> users = Collections.emptyList();
}

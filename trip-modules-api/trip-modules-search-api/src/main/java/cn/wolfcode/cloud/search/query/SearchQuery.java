package cn.wolfcode.cloud.search.query;

import cn.wolfcode.cloud.core.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchQuery extends QueryObject {

    private Integer type = -1;
}

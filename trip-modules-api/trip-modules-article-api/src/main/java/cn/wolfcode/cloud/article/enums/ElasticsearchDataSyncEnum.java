package cn.wolfcode.cloud.article.enums;

import lombok.Getter;

@Getter
public enum ElasticsearchDataSyncEnum {

    STRATEGY("strategies"),
    DESTINATION("destinations"),
    TRAVEL("travels"),
    USER_INFO("userInfos");

    private String name;

    ElasticsearchDataSyncEnum(String name) {
        this.name = name;
    }
}

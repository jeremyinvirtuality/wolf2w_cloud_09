package cn.wolfcode.cloud.article.events;

import cn.wolfcode.cloud.article.enums.ElasticsearchDataSyncEnum;
import cn.wolfcode.cloud.article.enums.OperateTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class StrategyDataSyncEvent extends ElasticsearchDataSyncEvent {

    private OperateTypeEnum operateType;

    public StrategyDataSyncEvent(OperateTypeEnum operateType, Object source) {
        super(ElasticsearchDataSyncEnum.STRATEGY, operateType, source);
        this.operateType = operateType;
    }
}

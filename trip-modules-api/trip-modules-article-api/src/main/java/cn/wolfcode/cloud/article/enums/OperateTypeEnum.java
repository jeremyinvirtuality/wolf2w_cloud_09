package cn.wolfcode.cloud.article.enums;

import lombok.Getter;

@Getter
public enum OperateTypeEnum {
    INSERT("insert"),
    UPDATE("update"),
    DELETE("delete");

    private String name;

    OperateTypeEnum(String name) {
        this.name = name;
    }
}

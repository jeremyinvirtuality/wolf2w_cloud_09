package cn.wolfcode.cloud.article.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 攻略
 * Domain => DO => 实体对象 => 结构与数据库相同，与数据库表做关系映射
 * Value Object => VO => 值对象 => 结构与数据库表不同，用于接收数据库查询的特殊结构数据 & 接收前端传入的参数
 * Business Object => BO => 业务对象 => 用于接收特定业务场景的参数的对象，实现参数校验等相关功能
 * Data Transfer Object => DTO => 数据传输对象 => 用于服务端内部子系统（微服务）之间的调用传输的参数对象
 */
@Setter
@Getter
@ToString
public class StrategyDTO implements Serializable {

    public static final int ABROAD_NO = 0;  //国内
    public static final int ABROAD_YES = 1;  //国外

    public static final int STATE_NORMAL = 0;  //带发布
    public static final int STATE_PUBLISH = 1; //发布

    private Long id;
    private String title;  //标题
    private String subTitle; //副标题
    private String summary;  //内容摘要
    private Long destId;  //关联的目的地
    private String destName;
    private Long themeId; //关联主题
    private String themeName;
    private Long catalogId;  //关联的分类
    private String catalogName;
    private String coverUrl;  //封面
    private Date createTime;  //创建时间
    private Integer isabroad = ABROAD_NO;  //是否是国外
    private Integer viewnum;  //点击数
    private Integer replynum;  //攻略评论数
    private Integer favornum; //收藏数
    private Integer sharenum; //分享数
    private Integer thumbsupnum; //点赞个数
    private Integer state;  //状态
}
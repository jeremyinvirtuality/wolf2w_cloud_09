package cn.wolfcode.cloud.article.events;

import cn.wolfcode.cloud.article.domain.Strategy;
import cn.wolfcode.cloud.article.enums.OperateTypeEnum;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * 1. 继承 ApplicationEvent 类
 * 2. 覆写父类构造器
 */
@Getter
public class StrategyEvent extends ApplicationEvent {

    private OperateTypeEnum type;
    private Strategy original;

    /**
     * 调用父类构造器
     *
     * @param source 事件源对象
     */
    public StrategyEvent(OperateTypeEnum type, Object source) {
        this(type, null, source);
    }

    public StrategyEvent(OperateTypeEnum type, Strategy original, Object source) {
        super(source);
        this.type = type;
        this.original = original;
    }
}

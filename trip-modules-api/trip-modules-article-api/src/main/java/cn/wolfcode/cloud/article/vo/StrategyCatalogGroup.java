package cn.wolfcode.cloud.article.vo;

import cn.wolfcode.cloud.article.domain.StrategyCatalog;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StrategyCatalogGroup {

    private String destName;
    private List<StrategyCatalog> catalogList;
}

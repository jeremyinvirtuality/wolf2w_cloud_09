package cn.wolfcode.cloud.article.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 游记推荐
 */
@ApiModel("轮播图实体对象")
@Setter
@Getter
@TableName("banner")
public class Banner implements Serializable {

    public static final int STATE_NORMAL = 0;   //正常
    public static final int STATE_DISABLE = 1;  //禁用

    public static final int TYPE_TRAVEL = 1;  //游记
    public static final int TYPE_STRATEGY = 2;  //攻略

    @TableId(type = IdType.AUTO)
    @ApiModelProperty("主键ID")
    private Long id;
    @ApiModelProperty(value = "引用id", notes = "根据 type 参数决定是攻略id还是游记id")
    private Long refid;  //关联id
    @ApiModelProperty("标题")
    private String title;  //标题
    @ApiModelProperty("副标题")
    private String subtitle; //副标题
    @ApiModelProperty("封面地址")
    private String coverUrl; //封面
    @ApiModelProperty(value = "状态", notes = "0=正常，1=异常")
    private Integer state = STATE_NORMAL; //状态
    @ApiModelProperty("排序")
    private Integer seq; //排序
    @ApiModelProperty(value = "类型", notes = "1=游记，2=攻略")
    private Integer type;
}
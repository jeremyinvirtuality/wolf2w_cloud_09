package cn.wolfcode.cloud.article.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 攻略
 */
@Setter
@Getter
@TableName("strategy_content")
public class StrategyContent implements Serializable {

    @TableId(type = IdType.INPUT)
    private Long id;
    private String content; //攻略内容

}
package cn.wolfcode.cloud.article.events;

import cn.wolfcode.cloud.article.enums.ElasticsearchDataSyncEnum;
import cn.wolfcode.cloud.article.enums.OperateTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class ElasticsearchDataSyncEvent extends ApplicationEvent {

    private ElasticsearchDataSyncEnum dataType;
    private OperateTypeEnum operateType;

    public ElasticsearchDataSyncEvent(ElasticsearchDataSyncEnum dataType, OperateTypeEnum operateType, Object source) {
        super(source);
        this.dataType = dataType;
        this.operateType = operateType;
    }
}

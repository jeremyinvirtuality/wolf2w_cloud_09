package cn.wolfcode.cloud.article.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 攻略条件统计表
 */
@Setter
@Getter
public class StrategyCondition {

    public static final int TYPE_ABROAD = 1;  //国外
    public static final int TYPE_CHINA = 2;   //国内
    public static final int TYPE_THEME = 3;   //主题

    public static final String ID_KEY = "id:";
    public static final String NAME_KEY = "name:";
    public static final String COUNT_KEY = "count:";
    public static final String TYPE_KEY = "type:";

    private Long id; //关联id
    private String name;
    private Integer count; //个数
    private Integer type; //条件类型
}
package cn.wolfcode.cloud.data.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/* 启用任务调度（定时任务） */
@EnableScheduling
@Configuration
public class ScheduledConfig {
}

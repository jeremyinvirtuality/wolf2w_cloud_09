package cn.wolfcode.cloud.data.jobs;

import cn.wolfcode.cloud.article.domain.StrategyRank;
import cn.wolfcode.cloud.core.util.R;
import cn.wolfcode.cloud.data.feign.ArticlesFeignService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class StaticsStrategyRankingJob {

    private final ArticlesFeignService articlesFeignService;

    /**
     * 每天 00:00:00 执行一次
     */
     @Scheduled(cron = "0 0 0 * * ?")
//    @Scheduled(cron = "0 * * * * ?")
    public void run() {
        log.info("[攻略排行数据统计] 统计数据开始.....");
        R<?> r = articlesFeignService.rankingByType(StrategyRank.TYPE_ABROAD);
        log.info("[攻略排行数据统计] 国外攻略数据统计完成: {}", r);
        r = articlesFeignService.rankingByType(StrategyRank.TYPE_CHINA);
        log.info("[攻略排行数据统计] 国内攻略数据统计完成: {}", r);
        r = articlesFeignService.rankingByType(StrategyRank.TYPE_HOT);
        log.info("[攻略排行数据统计] 热门攻略数据统计完成: {}", r);
        log.info("[攻略排行数据统计] 统计数据结束.....");
    }
}

package cn.wolfcode.cloud.data.feign;

import cn.wolfcode.cloud.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("trip-article")
public interface ArticlesFeignService {

    @PostMapping("/backend/statics/ranking/{type}")
    R<?> rankingByType(@PathVariable Integer type);
}

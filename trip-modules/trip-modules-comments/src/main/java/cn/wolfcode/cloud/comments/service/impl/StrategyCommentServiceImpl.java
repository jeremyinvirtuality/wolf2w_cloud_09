package cn.wolfcode.cloud.comments.service.impl;

import cn.wolfcode.cloud.auth.utils.AuthUtils;
import cn.wolfcode.cloud.auth.vo.LoginUser;
import cn.wolfcode.cloud.comments.domain.StrategyComment;
import cn.wolfcode.cloud.comments.mapper.StrategyCommentMapper;
import cn.wolfcode.cloud.comments.query.CommentQuery;
import cn.wolfcode.cloud.comments.service.StrategyCommentService;
import cn.wolfcode.cloud.redis.events.StrategyStatEvent;
import cn.wolfcode.cloud.redis.utils.SpringContextUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StrategyCommentServiceImpl extends ServiceImpl<StrategyCommentMapper, StrategyComment> implements StrategyCommentService {

    private final AuthUtils authUtils;

    @Override
    public boolean save(StrategyComment entity) {
        // 获取当前用户对象
        LoginUser user = authUtils.getLoginUser(true);
        // 设置用户信息
        entity.setUserId(user.getId());
        entity.setNickname(user.getNickname());
        entity.setLevel(user.getLevel());
        entity.setCity(user.getCity());
        entity.setHeadImgUrl(user.getHeadImgUrl());
        // 补充其他信息
        entity.setCreateTime(new Date());
        entity.setThumbupnum(0);
        boolean save = super.save(entity);
        if (save) {
            SpringContextUtil.publishEvent(new StrategyStatEvent(entity.getStrategyId(), "replynum", 1));
        }
        return save;
    }

    @Override
    public Page<StrategyComment> findPage(CommentQuery qo) {
        LambdaQueryWrapper<StrategyComment> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StrategyComment::getStrategyId, qo.getArticleId())
                .orderByDesc(StrategyComment::getCreateTime);

        Page<StrategyComment> page = page(new Page<>(qo.getCurrent(), qo.getSize()), wrapper);
        for (StrategyComment record : page.getRecords()) {
            List<Long> userIds = baseMapper.selectLikesByCid(record.getId());
            record.setThumbuplist(userIds);
        }
        return page;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void doLike(Long cid) {
        // 1. 获取当前登录用户
        LoginUser user = authUtils.getLoginUser(true);
        // 2. 根据登录用户+评论id查询是否点过赞
        List<Long> userIds = baseMapper.selectLikesByCid(cid);

        LambdaUpdateWrapper<StrategyComment> wrapper = new LambdaUpdateWrapper<>();
        if (userIds.contains(user.getId())) {
            // 3. 如果点过赞，就取消点赞（数量-1，删除中间表）
            baseMapper.deleteRelation(cid, user.getId());
            wrapper.setSql("thumbupnum=thumbupnum-1");
        } else {
            // 4. 如果没点过赞，就点赞（数量+1，插入中间表）
            baseMapper.insertRelation(cid, user.getId());
            wrapper.setSql("thumbupnum=thumbupnum+1");
        }

        wrapper.eq(StrategyComment::getId, cid);
        // 更新数据
        update(wrapper);
    }
}

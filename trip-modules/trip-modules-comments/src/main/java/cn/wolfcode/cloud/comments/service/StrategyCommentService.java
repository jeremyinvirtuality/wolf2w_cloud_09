package cn.wolfcode.cloud.comments.service;

import cn.wolfcode.cloud.comments.domain.StrategyComment;
import cn.wolfcode.cloud.comments.query.CommentQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

public interface StrategyCommentService extends IService<StrategyComment> {
    Page<StrategyComment> findPage(CommentQuery qo);

    void doLike(Long cid);
}

package cn.wolfcode.cloud.comments.mapper;

import cn.wolfcode.cloud.comments.domain.TravelComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TravelCommentMapper extends BaseMapper<TravelComment> {
}

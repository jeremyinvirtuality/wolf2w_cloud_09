package cn.wolfcode.cloud.comments.controller;

import cn.wolfcode.cloud.comments.domain.StrategyComment;
import cn.wolfcode.cloud.comments.query.CommentQuery;
import cn.wolfcode.cloud.comments.service.StrategyCommentService;
import cn.wolfcode.cloud.core.util.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/strategies")
@RequiredArgsConstructor
public class StrategyCommentController {

    private final StrategyCommentService strategyCommentService;

    @GetMapping("/query")
    public R<Page<StrategyComment>> findPage(CommentQuery qo) {
        return R.ok(strategyCommentService.findPage(qo));
    }

    @PostMapping
    public R<?> addComment(StrategyComment comment) {
        return R.toAjax(strategyCommentService.save(comment));
    }

    @PostMapping("/likes")
    public R<?> likes(Long cid) {
        strategyCommentService.doLike(cid);
        return R.ok();
    }
}

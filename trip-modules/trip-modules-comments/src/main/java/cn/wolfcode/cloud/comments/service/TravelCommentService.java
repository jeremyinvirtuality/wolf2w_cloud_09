package cn.wolfcode.cloud.comments.service;

import cn.wolfcode.cloud.comments.domain.TravelComment;
import cn.wolfcode.cloud.comments.query.CommentQuery;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface TravelCommentService extends IService<TravelComment> {
    List<TravelComment> findList(CommentQuery qo);
}

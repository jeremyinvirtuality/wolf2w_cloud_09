package cn.wolfcode.cloud.comments.service.impl;

import cn.wolfcode.cloud.auth.utils.AuthUtils;
import cn.wolfcode.cloud.auth.vo.LoginUser;
import cn.wolfcode.cloud.comments.domain.TravelComment;
import cn.wolfcode.cloud.comments.mapper.TravelCommentMapper;
import cn.wolfcode.cloud.comments.query.CommentQuery;
import cn.wolfcode.cloud.comments.service.TravelCommentService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TravelCommentServiceImpl extends ServiceImpl<TravelCommentMapper, TravelComment> implements TravelCommentService {

    private final AuthUtils authUtils;

    @Override
    public boolean save(TravelComment entity) {
        // 1. 获取当前用户
        LoginUser user = authUtils.getLoginUser(true);
        // 2. 设置用户信息
        entity.setLevel(user.getLevel());
        entity.setNickname(user.getNickname());
        entity.setCity(user.getCity());
        entity.setHeadImgUrl(user.getHeadImgUrl());
        entity.setUserId(user.getId());
        // 3. 回补其他信息
        entity.setCreateTime(new Date());
        entity.setType(TravelComment.TYPE_ARTICLE);
        if (entity.getRefCommentId() != null) {
            entity.setType(TravelComment.TYPE_REPLY);
        }
        // 4. 保存
        return super.save(entity);
    }

    @Override
    public List<TravelComment> findList(CommentQuery qo) {
        LambdaQueryWrapper<TravelComment> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TravelComment::getTravelId, qo.getArticleId())
                .orderByDesc(TravelComment::getCreateTime);

        List<TravelComment> comments = list(wrapper);
        for (TravelComment comment : comments) {
            // 查询引用的评论数据
            comment.setRefComment(getById(comment.getRefCommentId()));
        }
        return comments;
    }
}

package cn.wolfcode.cloud.comments.controller;

import cn.wolfcode.cloud.comments.domain.TravelComment;
import cn.wolfcode.cloud.comments.query.CommentQuery;
import cn.wolfcode.cloud.comments.service.TravelCommentService;
import cn.wolfcode.cloud.core.util.R;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/travels")
@RequiredArgsConstructor
public class TravelCommentController {

    private final TravelCommentService travelCommentService;

    @GetMapping("/query")
    public R<List<TravelComment>> findPage(CommentQuery qo) {
        return R.ok(travelCommentService.findList(qo));
    }

    @PostMapping
    public R<?> addComment(TravelComment comment) {
        return R.toAjax(travelCommentService.save(comment));
    }
}

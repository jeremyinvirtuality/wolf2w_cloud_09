package cn.wolfcode.cloud.comments.controller;

import cn.wolfcode.cloud.auth.anno.RequestUser;
import cn.wolfcode.cloud.auth.vo.LoginUser;
import cn.wolfcode.cloud.core.util.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test/args")
public class TestArgumentController {

    @GetMapping
    public R<LoginUser> test(Integer aa, @RequestUser LoginUser user) {
        return R.ok(user);
    }

    @GetMapping("/v2")
    public R<LoginUser> test2(Integer aa, @RequestUser(required = false) LoginUser user) {
        return R.ok(user);
    }

    @GetMapping("/v3")
    public R<LoginUser> test3(Integer aa, LoginUser user) {
        return R.ok(user);
    }
}

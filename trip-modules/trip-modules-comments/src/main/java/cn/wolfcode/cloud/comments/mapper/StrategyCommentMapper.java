package cn.wolfcode.cloud.comments.mapper;

import cn.wolfcode.cloud.comments.domain.StrategyComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StrategyCommentMapper extends BaseMapper<StrategyComment> {
    List<Long> selectLikesByCid(Long cid);

    void deleteRelation(@Param("commentId") Long commentId, @Param("userId") Long userId);

    int insertRelation(@Param("commentId") Long commentId, @Param("userId") Long userId);
}

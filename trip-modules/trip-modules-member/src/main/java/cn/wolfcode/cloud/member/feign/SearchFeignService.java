package cn.wolfcode.cloud.member.feign;

import cn.wolfcode.clcoud.domain.UserInfo;
import cn.wolfcode.cloud.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("trip-search")
public interface SearchFeignService {

    @PostMapping("/userInfos/batch")
    R<?> batchSaveUserInfo(@RequestBody List<UserInfo> userInfos);
}

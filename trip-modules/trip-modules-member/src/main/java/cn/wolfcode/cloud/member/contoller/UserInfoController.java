package cn.wolfcode.cloud.member.contoller;

import cn.wolfcode.clcoud.domain.UserInfo;
import cn.wolfcode.clcoud.vo.RegisterParam;
import cn.wolfcode.cloud.auth.anno.RequestUser;
import cn.wolfcode.cloud.auth.vo.LoginResp;
import cn.wolfcode.cloud.auth.vo.LoginUser;
import cn.wolfcode.cloud.core.util.R;
import cn.wolfcode.cloud.member.service.SmsService;
import cn.wolfcode.cloud.member.service.UserInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserInfoController {

    private final UserInfoService userInfoService;
    private final SmsService smsService;

    @GetMapping("/findByIdList")
    public List<UserInfo> findByIdList(@RequestParam Set<Long> idList) {
        return userInfoService.listByIds(idList);
    }

    @GetMapping("/findFavoritesByUserId")
    public List<Long> findFavoritesByUserId(@RequestParam Long id) {
        return userInfoService.findFavoritesByUserId(id);
    }

    @PostMapping("/favor/strategies")
    public R<Boolean> favorStrategy(Long strategyId, @RequestUser LoginUser user) {
        return R.ok(userInfoService.doFavorStrategy(strategyId, user.getId()));
    }

    @PostMapping("/login")
    public R<LoginResp> login(String username, String password) {
        LoginResp resp = userInfoService.login(username, password);
        return R.ok(resp);
    }

    @PostMapping("/refreshToken")
    public R<LoginResp> refreshToken(@RequestHeader("Authorization") String token) {
        LoginResp resp = userInfoService.doRefreshToken(token);
        return R.ok(resp);
    }

    @GetMapping("/exists")
    public R<Boolean> phoneExists(String phone) {
        UserInfo userInfo = userInfoService.findByPhone(phone);
        return R.ok(userInfo != null);
    }

    @PostMapping("/register")
    public R<?> register(RegisterParam param) {
        userInfoService.doRegister(param);
        return R.ok();
    }

    @PostMapping("/VerifyCode")
    public R<?> getVerifyCode(String phone) {
        smsService.registerSmsSend(phone);
        return R.ok();
    }
}

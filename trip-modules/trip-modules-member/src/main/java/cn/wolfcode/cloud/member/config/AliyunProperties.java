package cn.wolfcode.cloud.member.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@Getter
@Setter
@ConfigurationProperties(prefix = "aliyun")
public class AliyunProperties {

    private AccessKeyConfig accessKey;
    private SmsConfig sms;

    @Getter
    @Setter
    public static class AccessKeyConfig {
        private String id;
        private String secret;
    }

    @Getter
    @Setter
    public static class SmsConfig {
        private boolean enable;
        private String endpoint;
        private Map<String, SmsTemplate> templates;
    }

    @Getter
    @Setter
    public static class SmsTemplate {
        private String signName;
        private String templateCode;
    }
}

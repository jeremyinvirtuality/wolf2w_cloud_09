package cn.wolfcode.cloud.member.contoller;

import cn.wolfcode.cloud.auth.anno.RequiredLogin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredLogin
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/index")
    public String test1() {
        return "111111";
    }
}

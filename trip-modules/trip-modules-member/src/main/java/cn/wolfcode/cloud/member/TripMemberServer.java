package cn.wolfcode.cloud.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@MapperScan("cn.wolfcode.cloud.member.mapper")
@SpringBootApplication
public class TripMemberServer {

    public static void main(String[] args) {
        SpringApplication.run(TripMemberServer.class, args);
    }
}

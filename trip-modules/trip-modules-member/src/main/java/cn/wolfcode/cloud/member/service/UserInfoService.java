package cn.wolfcode.cloud.member.service;

import cn.wolfcode.clcoud.domain.UserInfo;
import cn.wolfcode.clcoud.vo.RegisterParam;
import cn.wolfcode.cloud.auth.vo.LoginResp;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface UserInfoService extends IService<UserInfo> {
    UserInfo findByPhone(String phone);

    void doRegister(RegisterParam param);

    LoginResp login(String username, String password);

    LoginResp doRefreshToken(String token);

    Boolean doFavorStrategy(Long strategyId, Long id);

    List<Long> findFavoritesByUserId(Long id);
}

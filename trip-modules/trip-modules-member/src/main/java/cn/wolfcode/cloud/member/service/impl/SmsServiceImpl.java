package cn.wolfcode.cloud.member.service.impl;

import cn.wolfcode.cloud.core.util.AssertUtils;
import cn.wolfcode.cloud.member.config.AliyunProperties;
import cn.wolfcode.cloud.member.service.SmsService;
import cn.wolfcode.cloud.redis.key.RedisKeyEnum;
import cn.wolfcode.cloud.redis.service.RedisService;
import com.alibaba.fastjson2.JSON;
import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.tea.TeaException;
import com.aliyun.teautil.models.RuntimeOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Random;

@Slf4j
@RefreshScope
@Service
@RequiredArgsConstructor
public class SmsServiceImpl implements SmsService {
    private final RedisService redisService;
    private final Client smsClient;
    private final AliyunProperties aliyunProperties;

    public static final String TEMPLATE_ID_REGISTER = "REGISTER";
    public static final String TEMPLATE_ID_RESET_PWD = "RESET_PWD";

    @Override
    public void registerSmsSend(String phone) {
        // 1. 生成验证码
        // String substring = UUID.randomUUID().toString().substring(0, 4);
        Random random = new Random();
        StringBuilder sb = new StringBuilder(10);
        for (int i = 0; i < 6; i++) {
            sb.append(random.nextInt(10));
        }
        String verifyCode = sb.toString();
        // 2. 保存验证码（时效性，性能）
        redisService.set(RedisKeyEnum.REGISTER_SMS_VERIFY_CODE, verifyCode, phone);

        // 3. TODO 将验证码发送到对方手机
        System.out.println("发送短信到手机：" + verifyCode);
        try {
            aliyunSmsSend(TEMPLATE_ID_REGISTER, phone, verifyCode);
        } catch (Exception e) {
            log.error("[短信服务] 发送阿里云短信异常", e);
        }
    }

    public void aliyunSmsSend(String templateId, String phone, String code) throws Exception {
        AliyunProperties.SmsConfig sms = aliyunProperties.getSms();
        if (sms.isEnable()) {
            Map<String, AliyunProperties.SmsTemplate> templates = sms.getTemplates();
            AliyunProperties.SmsTemplate template = templates.get(templateId);
            AssertUtils.notNull(template, "短信模板配置错误");

            SendSmsRequest sendSmsRequest = new SendSmsRequest()
                    .setSignName(template.getSignName())
                    .setTemplateCode(template.getTemplateCode())
                    .setPhoneNumbers(phone)
                    .setTemplateParam("{\"code\":\"" + code + "\"}");
            RuntimeOptions runtime = new RuntimeOptions();
            try {
                SendSmsResponse sendSmsResponse = smsClient.sendSmsWithOptions(sendSmsRequest, runtime);
                System.out.println(JSON.toJSONString(sendSmsResponse.getBody()));
            } catch (TeaException error) {
                System.out.println(error.getMessage());
                System.out.println(error.getData().get("Recommend"));
                com.aliyun.teautil.Common.assertAsString(error.message);
            } catch (Exception _error) {
                TeaException error = new TeaException(_error.getMessage(), _error);
                System.out.println(error.getMessage());
                System.out.println(error.getData().get("Recommend"));
                com.aliyun.teautil.Common.assertAsString(error.message);
            }
        }
    }
}

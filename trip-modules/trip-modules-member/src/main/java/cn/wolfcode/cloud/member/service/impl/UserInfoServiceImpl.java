package cn.wolfcode.cloud.member.service.impl;

import cn.wolfcode.clcoud.domain.UserInfo;
import cn.wolfcode.clcoud.vo.RegisterParam;
import cn.wolfcode.cloud.auth.config.JwtAuthProperties;
import cn.wolfcode.cloud.auth.vo.LoginResp;
import cn.wolfcode.cloud.auth.vo.LoginUser;
import cn.wolfcode.cloud.core.exception.ServiceException;
import cn.wolfcode.cloud.core.util.AssertUtils;
import cn.wolfcode.cloud.member.mapper.UserInfoMapper;
import cn.wolfcode.cloud.member.service.UserInfoService;
import cn.wolfcode.cloud.redis.events.StrategyStatEvent;
import cn.wolfcode.cloud.redis.key.RedisKeyEnum;
import cn.wolfcode.cloud.redis.service.RedisService;
import cn.wolfcode.cloud.redis.utils.SpringContextUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.Md5Crypt;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

    private final RedisService redisService;
    private final JwtAuthProperties jwtAuthProperties;

    @Override
    public UserInfo findByPhone(String phone) {
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<UserInfo>()
                .eq(UserInfo::getPhone, phone);
        return getOne(wrapper);
    }

    @Override
    public void doRegister(RegisterParam param) {
        // 1. 验证手机号是否重复
        UserInfo userInfo = findByPhone(param.getPhone());
        AssertUtils.isNull(userInfo, "手机号已注册");

        // 2. 从 redis 中获取验证码
        String verifyCode = redisService.get(RedisKeyEnum.REGISTER_SMS_VERIFY_CODE, param.getPhone());
        // 3. 判断验证码是否正确
        AssertUtils.equalsIgnoreCase(param.getVerifyCode(), verifyCode, "验证码错误");

        // 使用完成，删除验证码
        redisService.del(RedisKeyEnum.REGISTER_SMS_VERIFY_CODE, param.getPhone());

        // 将参数对象的数据拷贝到目标对象中
        userInfo = new UserInfo();
        BeanUtils.copyProperties(param, userInfo);

        String encryptPwd = encryptPwd(userInfo.getPassword(), userInfo.getPhone());
        userInfo.setPassword(encryptPwd);

        userInfo.setHeadImgUrl("/images/default.jpg"); // 默认头像

        // 4. 保存账户资料
        baseMapper.insert(userInfo);
    }

    @Override
    public LoginResp login(String username, String password) {
        // 1. 基于用户名查询用户对象
        UserInfo userInfo = findByPhone(username);
        AssertUtils.notNull(userInfo, "用户名或密码错误");

        // 2. 判断密码是否正确
        String encryptPwd = encryptPwd(password, username);
        AssertUtils.equals(userInfo.getPassword(), encryptPwd, "用户名或密码错误");

        LoginUser loginUser = new LoginUser();
        BeanUtils.copyProperties(userInfo, loginUser);

        // 3. 生成 token，存入 redis
        String accessToken = this.buildToken(jwtAuthProperties.getJwt().getExpiration(), loginUser);
        String refreshToken = this.buildToken(60L, loginUser);

        loginUser.setToken(accessToken);
        loginUser.setLoginTime(new Date());
        redisService.set(RedisKeyEnum.LOGIN_USER_KEY, loginUser, userInfo.getPhone());

        // 4. 返回 token, 登录用户信息
        return new LoginResp(accessToken, refreshToken, loginUser);
    }

    @Override
    public LoginResp doRefreshToken(String token) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(jwtAuthProperties.getJwt().getPrivateKey())
                    .parseClaimsJws(token)
                    .getBody();
            // 从 token 中获取用户手机号
            String phone = claims.getSubject();
            LoginUser loginUser = redisService.get(RedisKeyEnum.LOGIN_USER_KEY, phone);
            if (loginUser == null) {
                throw new ServiceException(401, "token 已过期，请重新登录");
            }

            String accessToken = this.buildToken(jwtAuthProperties.getJwt().getExpiration(), loginUser);
            String refreshToken = this.buildToken(60L, loginUser);

            loginUser.setToken(accessToken);
            loginUser.setLoginTime(new Date());
            redisService.set(RedisKeyEnum.LOGIN_USER_KEY, loginUser, phone);

            return new LoginResp(accessToken, refreshToken, loginUser);
        } catch (Exception e) {
            throw new ServiceException(401, "token 已过期，请重新登录");
        }
    }

    @Override
    public Boolean doFavorStrategy(Long strategyId, Long userId) {
        // 1. 查询用户收藏的所有攻略合集
        List<Long> favorites = baseMapper.selectFavoritesByUserId(userId);
        boolean favor = true;
        // 2. 判断用户是否已经收藏过该攻略
        StrategyStatEvent event = new StrategyStatEvent(strategyId, "favornum", -1);
        if (favorites.contains(strategyId)) {
            // 3. 如果已收藏，就取消收藏
            baseMapper.deleteFavorStrategy(userId, strategyId);
            favor = false;
        } else {
            // 4. 如果未收藏，就收藏
            baseMapper.insertFavorStrategy(userId, strategyId);
            event.setNum(1);
        }
        SpringContextUtil.publishEvent(event);
        return favor;
    }

    @Override
    public List<Long> findFavoritesByUserId(Long id) {
        return baseMapper.selectFavoritesByUserId(id);
    }

    private String buildToken(Long expiration, LoginUser user) {
        // JWT 封装的存储数据的对象，其实本质就是一个 HashMap
        DefaultClaims claims = new DefaultClaims();
        JwtAuthProperties.JwtConfig jwt = jwtAuthProperties.getJwt();
        long exp = System.currentTimeMillis() + expiration * 60 * 1000;
//        long expiration = System.currentTimeMillis() + 60 * 1000;
        claims.setExpiration(new Date(exp)); // 设置过期时间
        claims.setSubject(user.getPhone()); // 用户名
        claims.setId(user.getId() + ""); // 用户 id

        return Jwts.builder()
                .setClaims(claims) // 要存进 JWT 的数据
                .signWith(SignatureAlgorithm.forName(jwt.getAlgo()), jwt.getPrivateKey()).compact();
    }

    private static String encryptPwd(String password, String phone) {
        return Md5Crypt.md5Crypt(password.getBytes(StandardCharsets.UTF_8), "$1$al290k" + phone.substring(2, 8));
    }

    public static void main(String[] args) {
        String pwd = "123456";
        String s = encryptPwd(pwd, "13000000000");
        System.out.println("s = " + s);
    }
}

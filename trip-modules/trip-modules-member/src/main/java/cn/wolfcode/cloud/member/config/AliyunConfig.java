package cn.wolfcode.cloud.member.config;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.teaopenapi.models.Config;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(AliyunProperties.class)
public class AliyunConfig {

    @Bean
    public Client smsClient(AliyunProperties properties) throws Exception {
        AliyunProperties.AccessKeyConfig accessKey = properties.getAccessKey();
        Config config = new Config()
                .setAccessKeyId(accessKey.getId())
                .setAccessKeySecret(accessKey.getSecret());
        AliyunProperties.SmsConfig sms = properties.getSms();
        config.endpoint = sms.getEndpoint();
        return new Client(config);
    }
}

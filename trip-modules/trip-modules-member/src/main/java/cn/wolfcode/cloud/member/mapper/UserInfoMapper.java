package cn.wolfcode.cloud.member.mapper;

import cn.wolfcode.clcoud.domain.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserInfoMapper extends BaseMapper<UserInfo> {
    List<Long> selectFavoritesByUserId(Long userId);

    void insertFavorStrategy(@Param("userId") Long userId, @Param("strategyId") Long strategyId);

    void deleteFavorStrategy(@Param("userId") Long userId, @Param("strategyId") Long strategyId);
}

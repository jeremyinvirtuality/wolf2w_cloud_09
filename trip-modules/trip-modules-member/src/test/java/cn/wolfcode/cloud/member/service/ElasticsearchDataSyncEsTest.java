package cn.wolfcode.cloud.member.service;

import cn.wolfcode.clcoud.domain.UserInfo;
import cn.wolfcode.cloud.member.feign.SearchFeignService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@SpringBootTest
public class ElasticsearchDataSyncEsTest {

    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private SearchFeignService searchFeignService;

    @Test
    public void testBatchSaveUserInfo() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("batch-save");
        List<UserInfo> strategies = userInfoService.list();
        final AtomicInteger count = new AtomicInteger(0);

        List<UserInfo> batchSave = new ArrayList<>(1000);
        for (int i = 0; i < strategies.size(); i++) {
            batchSave.add(strategies.get(i));
            int current = count.incrementAndGet();
            if (current % 1000 == 0 || i == strategies.size() - 1) {
                // 保存一次
                searchFeignService.batchSaveUserInfo(batchSave);
                batchSave.clear();
            }
        }
        stopWatch.stop();
        System.out.println("任务执行时间 = " + stopWatch.getTotalTimeSeconds());
    }
}

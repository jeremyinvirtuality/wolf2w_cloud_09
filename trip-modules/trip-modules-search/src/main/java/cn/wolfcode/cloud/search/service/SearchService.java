package cn.wolfcode.cloud.search.service;

import cn.wolfcode.cloud.search.query.SearchQuery;
import org.springframework.data.domain.Page;

public interface SearchService<T> {

    Page<T> queryWithHighlight(Class<T> searchClass, SearchQuery qo, String... fields);
}
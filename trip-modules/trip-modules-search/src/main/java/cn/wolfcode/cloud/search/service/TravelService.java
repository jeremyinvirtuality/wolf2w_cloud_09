package cn.wolfcode.cloud.search.service;

import cn.wolfcode.cloud.article.dto.TravelDTO;
import cn.wolfcode.cloud.search.domain.TravelEs;

import java.util.List;

public interface TravelService extends SearchService<TravelEs> {

    void save(TravelDTO travel);

    void saveBatch(List<TravelDTO> travels);

    List<TravelEs> findListByDestName(String destName);
}

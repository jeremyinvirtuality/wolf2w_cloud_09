package cn.wolfcode.cloud.search.controller;

import cn.wolfcode.clcoud.dto.UserInfoDTO;
import cn.wolfcode.cloud.core.util.R;
import cn.wolfcode.cloud.search.service.UserInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/userInfos")
@RequiredArgsConstructor
public class UserInfoController {

    private final UserInfoService userInfoService;

    @PostMapping
    public R<?> save(@RequestBody UserInfoDTO userInfo) {
        userInfoService.save(userInfo);
        return R.ok();
    }

    @PostMapping("/batch")
    public R<?> batchSave(@RequestBody List<UserInfoDTO> strategies) {
        userInfoService.saveBatch(strategies);
        return R.ok();
    }
}

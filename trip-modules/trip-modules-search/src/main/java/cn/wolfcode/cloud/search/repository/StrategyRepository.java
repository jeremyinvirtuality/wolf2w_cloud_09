package cn.wolfcode.cloud.search.repository;

import cn.wolfcode.cloud.search.domain.StrategyEs;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface StrategyRepository extends ElasticsearchRepository<StrategyEs, Long> {
}

package cn.wolfcode.cloud.search.service.impl;

import cn.wolfcode.clcoud.dto.UserInfoDTO;
import cn.wolfcode.cloud.search.domain.UserInfoEs;
import cn.wolfcode.cloud.search.repository.UserInfoRepository;
import cn.wolfcode.cloud.search.service.UserInfoService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.BeanUtils;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserInfoServiceImpl extends SearchServiceImpl<UserInfoEs> implements UserInfoService {

    private final UserInfoRepository userInfoRepository;
    private final ElasticsearchRestTemplate elasticsearchRestTemplate;
    private final ThreadPoolExecutor dataSyncThreadPoolExecutor;

    public UserInfoServiceImpl(ElasticsearchRestTemplate elasticsearchRestTemplate, UserInfoRepository userInfoRepository, ElasticsearchRestTemplate elasticsearchRestTemplate1, ThreadPoolExecutor dataSyncThreadPoolExecutor) {
        super(elasticsearchRestTemplate);
        this.userInfoRepository = userInfoRepository;
        this.elasticsearchRestTemplate = elasticsearchRestTemplate1;
        this.dataSyncThreadPoolExecutor = dataSyncThreadPoolExecutor;
    }

    @Override
    public void save(UserInfoDTO userInfo) {
        UserInfoEs userInfoEs = new UserInfoEs();
        // 不推荐，性能差 => 推荐使用 MapStruct
        BeanUtils.copyProperties(userInfo, userInfoEs);
        log.info("[会员服务] 保存数据到 es: {}", userInfo.getId());
        userInfoRepository.save(userInfoEs);
    }

    @Override
    public void saveBatch(List<UserInfoDTO> strategies) {
        dataSyncThreadPoolExecutor.execute(() -> {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            List<UserInfoEs> collect = strategies.stream().map(userInfo -> {
                UserInfoEs userInfoEs = new UserInfoEs();
                // 不推荐，性能差 => 推荐使用 MapStruct
                BeanUtils.copyProperties(userInfo, userInfoEs);
                return userInfoEs;
            }).collect(Collectors.toList());
            userInfoRepository.saveAll(collect);
            stopWatch.stop();
            log.info("[会员服务] 批量保存数据：耗时：{}s, 数量：{}", stopWatch.getTotalTimeSeconds(), strategies.size());
        });
    }

    @Override
    public List<UserInfoEs> findListByDestName(String destName) {
        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchPhraseQuery("city", destName)).build();
        SearchHits<UserInfoEs> search = elasticsearchRestTemplate.search(query, UserInfoEs.class);
        return search.getSearchHits().stream().map(SearchHit::getContent).collect(Collectors.toList());
    }
}

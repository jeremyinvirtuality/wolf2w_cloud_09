package cn.wolfcode.cloud.search.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

@Slf4j
@Configuration
public class AppConfig {

    @Bean
    public ThreadPoolExecutor dataSyncThreadPoolExecutor() {
        // 创建线程工厂
        ThreadFactory threadFactory = new ThreadFactory() {

            private final LongAdder threadCount = new LongAdder();

            @Override
            public Thread newThread(Runnable r) {
                threadCount.increment();
                Thread thread = new Thread(r, "data-sync-" + threadCount.intValue());
                thread.setDaemon(false);
                thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread t, Throwable e) {
                        log.error("[数据同步线程] 子线程运行异常", e);
                    }
                });
                return thread;
            }
        };

        int processors = Runtime.getRuntime().availableProcessors();
        return new ThreadPoolExecutor(
                processors,
                processors * 2,
                1000,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(100),
                threadFactory,
                // 谁发布的任务，谁自己执行
                new ThreadPoolExecutor.CallerRunsPolicy()
        );
    }
}

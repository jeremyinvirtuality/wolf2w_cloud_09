package cn.wolfcode.cloud.search.controller;

import cn.wolfcode.cloud.article.dto.StrategyDTO;
import cn.wolfcode.cloud.core.util.R;
import cn.wolfcode.cloud.search.service.StrategyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/strategies")
@RequiredArgsConstructor
public class StrategyController {

    private final StrategyService strategyService;

    @PostMapping("/data/sync/insert")
    public R<?> insert(@RequestBody StrategyDTO data) {
        strategyService.save(data);
        return R.ok();
    }

    @PostMapping("/data/sync/update")
    public R<?> updateById(@RequestBody StrategyDTO data) {
        strategyService.updateById(data);
        return R.ok();
    }

    @PostMapping("/data/sync/delete")
    public R<?> deleteById(@RequestBody Long data) {
        strategyService.deleteById(data);
        return R.ok();
    }

    @PostMapping
    public R<?> save(@RequestBody StrategyDTO strategy) {
        strategyService.save(strategy);
        return R.ok();
    }

    @PostMapping("/batch")
    public R<?> batchSave(@RequestBody List<StrategyDTO> strategies) {
        strategyService.saveBatch(strategies);
        return R.ok();
    }
}

package cn.wolfcode.cloud.search.controller;

import cn.wolfcode.cloud.article.dto.DestinationDTO;
import cn.wolfcode.cloud.core.util.R;
import cn.wolfcode.cloud.search.service.DestinationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/destinations")
@RequiredArgsConstructor
public class DestinationController {

    private final DestinationService destinationService;

    @PostMapping
    public R<?> save(@RequestBody DestinationDTO destination) {
        destinationService.save(destination);
        return R.ok();
    }

    @PostMapping("/batch")
    public R<?> batchSave(@RequestBody List<DestinationDTO> strategies) {
        destinationService.saveBatch(strategies);
        return R.ok();
    }
}

package cn.wolfcode.cloud.search.service;

import cn.wolfcode.cloud.article.dto.DestinationDTO;
import cn.wolfcode.cloud.search.domain.DestinationEs;

import java.util.List;

public interface DestinationService extends SearchService<DestinationEs> {

    void save(DestinationDTO destination);

    void saveBatch(List<DestinationDTO> destinations);

    DestinationEs findByName(String name);
}

package cn.wolfcode.cloud.search.service;

import cn.wolfcode.clcoud.dto.UserInfoDTO;
import cn.wolfcode.cloud.search.domain.UserInfoEs;

import java.util.List;

public interface UserInfoService extends SearchService<UserInfoEs> {

    void save(UserInfoDTO userInfo);

    void saveBatch(List<UserInfoDTO> userInfos);

    List<UserInfoEs> findListByDestName(String destName);
}

package cn.wolfcode.cloud.search.service.impl;

import cn.wolfcode.cloud.article.dto.DestinationDTO;
import cn.wolfcode.cloud.search.domain.DestinationEs;
import cn.wolfcode.cloud.search.repository.DestinationRepository;
import cn.wolfcode.cloud.search.service.DestinationService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.BeanUtils;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DestinationServiceImpl extends SearchServiceImpl<DestinationEs> implements DestinationService {

    private final DestinationRepository destinationRepository;
    private final ElasticsearchRestTemplate elasticsearchRestTemplate;
    private final ThreadPoolExecutor dataSyncThreadPoolExecutor;

    public DestinationServiceImpl(ElasticsearchRestTemplate elasticsearchRestTemplate, DestinationRepository destinationRepository, ElasticsearchRestTemplate elasticsearchRestTemplate1, ThreadPoolExecutor dataSyncThreadPoolExecutor) {
        super(elasticsearchRestTemplate);
        this.destinationRepository = destinationRepository;
        this.elasticsearchRestTemplate = elasticsearchRestTemplate1;
        this.dataSyncThreadPoolExecutor = dataSyncThreadPoolExecutor;
    }

    @Override
    public void save(DestinationDTO destination) {
        DestinationEs destinationEs = new DestinationEs();
        // 不推荐，性能差 => 推荐使用 MapStruct
        BeanUtils.copyProperties(destination, destinationEs);
        log.info("[目的地服务] 保存数据到 es: {}", destination.getId());
        destinationRepository.save(destinationEs);
    }

    @Override
    public void saveBatch(List<DestinationDTO> strategies) {
        dataSyncThreadPoolExecutor.execute(() -> {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            List<DestinationEs> collect = strategies.stream().map(destination -> {
                DestinationEs destinationEs = new DestinationEs();
                // 不推荐，性能差 => 推荐使用 MapStruct
                BeanUtils.copyProperties(destination, destinationEs);
                return destinationEs;
            }).collect(Collectors.toList());
            destinationRepository.saveAll(collect);
            stopWatch.stop();
            log.info("[目的地服务] 批量保存数据：耗时：{}s, 数量：{}", stopWatch.getTotalTimeSeconds(), strategies.size());
        });
    }

    @Override
    public DestinationEs findByName(String name) {
        // 构建条件对象
        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(
                        QueryBuilders.matchPhraseQuery("name", name)
                ).build();

//        Optional<SearchHit<DestinationEs>> optional =
//                Optional.ofNullable(elasticsearchRestTemplate.searchOne(query, DestinationEs.class));
//
//        return optional.map(SearchHit::getContent).orElse(null);

        SearchHit<DestinationEs> searchHit = elasticsearchRestTemplate.searchOne(query, DestinationEs.class);
        return searchHit == null ? null : searchHit.getContent();
    }
}

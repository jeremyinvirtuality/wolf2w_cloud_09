package cn.wolfcode.cloud.search.controller;

import cn.wolfcode.cloud.core.util.R;
import cn.wolfcode.cloud.search.domain.DestinationEs;
import cn.wolfcode.cloud.search.domain.StrategyEs;
import cn.wolfcode.cloud.search.domain.TravelEs;
import cn.wolfcode.cloud.search.domain.UserInfoEs;
import cn.wolfcode.cloud.search.query.SearchQuery;
import cn.wolfcode.cloud.search.service.DestinationService;
import cn.wolfcode.cloud.search.service.StrategyService;
import cn.wolfcode.cloud.search.service.TravelService;
import cn.wolfcode.cloud.search.service.UserInfoService;
import cn.wolfcode.cloud.search.vo.SearchResult;
import com.alibaba.fastjson2.JSONObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URLDecoder;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/q")
@RequiredArgsConstructor
public class SearchController {

    private final DestinationService destinationService;
    private final StrategyService strategyService;
    private final TravelService travelService;
    private final UserInfoService userInfoService;

    @GetMapping
    public R<?> search(SearchQuery qo) {
        try {
            // 对 keyword 进行解码
            if (StringUtils.isNotEmpty(qo.getKeyword())) {
                qo.setKeyword(URLDecoder.decode(qo.getKeyword(), "UTF-8"));
            }

            JSONObject json = null;
            switch (qo.getType()) {
                case 0:
                    // 查询目的地
                    json = this.searchWithDestination(qo);
                    break;
                case 1:
                    // 查询攻略
                    json = this.searchWithStrategy(qo);
                    break;
                case 2:
                    // 查询游记
                    json = this.searchWithTravel(qo);
                    break;
                case 3:
                    // 查询用户
                    json = this.searchWithUserInfo(qo);
                    break;
                default:
                    // 查询所有
                    json = this.searchWithAll(qo);
                    break;
            }
            json.put("qo", qo);
            return R.ok(json);
        } catch (Exception e) {
            log.info("[搜索服务] 搜索出现异常", e);
            return R.error(500, e.getMessage());
        }
    }

    private JSONObject searchWithAll(SearchQuery qo) {
        SearchResult result = new SearchResult();
        // 查询目的地
        Page<DestinationEs> destinationEsPage =
                destinationService.queryWithHighlight(DestinationEs.class, qo, "name", "info");
        result.setDestinations(destinationEsPage.getContent());
        result.setTotal(destinationEsPage.getTotalElements());
        // 查询攻略
        Page<StrategyEs> strategyEsPage =
                strategyService.queryWithHighlight(StrategyEs.class, qo, "title", "subTitle", "summary");
        result.setStrategies(strategyEsPage.getContent());
        result.setTotal(result.getTotal() + strategyEsPage.getTotalElements());
        // 查询游记
        Page<TravelEs> travelEsPage =
                travelService.queryWithHighlight(TravelEs.class, qo, "title", "summary");
        result.setTravels(travelEsPage.getContent());
        result.setTotal(result.getTotal() + travelEsPage.getTotalElements());
        // 查询用户
        Page<UserInfoEs> userInfoEsPage =
                userInfoService.queryWithHighlight(UserInfoEs.class, qo, "info", "city");
        result.setUsers(userInfoEsPage.getContent());
        result.setTotal(result.getTotal() + userInfoEsPage.getTotalElements());
        return JSONObject.of("result", result);
    }

    private JSONObject searchWithUserInfo(SearchQuery qo) {
        Page<UserInfoEs> page =
                userInfoService.queryWithHighlight(UserInfoEs.class, qo, "info", "city");
        return JSONObject.of("page", page);
    }

    private JSONObject searchWithTravel(SearchQuery qo) {
        Page<TravelEs> page =
                travelService.queryWithHighlight(TravelEs.class, qo, "title", "summary");
        return JSONObject.of("page", page);
    }

    private JSONObject searchWithStrategy(SearchQuery qo) {
        Page<StrategyEs> page =
                strategyService.queryWithHighlight(StrategyEs.class, qo, "title", "subTitle", "summary");
        return JSONObject.of("page", page);
    }

    private JSONObject searchWithDestination(SearchQuery qo) {
        DestinationEs destination = destinationService.findByName(qo.getKeyword());
        SearchResult result = new SearchResult();
        if (destination != null) {
            // 基于目的地名称查询攻略
            List<StrategyEs> strategies = strategyService.findListByDestName(qo.getKeyword());
            result.setStrategies(strategies);
            // 基于目的地名称查询游记
            List<TravelEs> travels = travelService.findListByDestName(qo.getKeyword());
            result.setTravels(travels);
            // 基于目的地名称查询用户
            List<UserInfoEs> users = userInfoService.findListByDestName(qo.getKeyword());
            result.setUsers(users);

            // 设置总数
            result.setTotal(1L + strategies.size() + travels.size() + users.size());
        }
        return JSONObject.of("result", result, "dest", destination);
    }
}

package cn.wolfcode.cloud.search.repository;

import cn.wolfcode.cloud.search.domain.UserInfoEs;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface UserInfoRepository extends ElasticsearchRepository<UserInfoEs, Long> {
}

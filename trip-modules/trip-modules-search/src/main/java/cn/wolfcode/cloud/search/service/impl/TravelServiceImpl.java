package cn.wolfcode.cloud.search.service.impl;

import cn.wolfcode.cloud.article.dto.TravelDTO;
import cn.wolfcode.cloud.search.domain.TravelEs;
import cn.wolfcode.cloud.search.repository.TravelRepository;
import cn.wolfcode.cloud.search.service.TravelService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.BeanUtils;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TravelServiceImpl extends SearchServiceImpl<TravelEs> implements TravelService {

    private final TravelRepository travelRepository;
    private final ElasticsearchRestTemplate elasticsearchRestTemplate;
    private final ThreadPoolExecutor dataSyncThreadPoolExecutor;

    public TravelServiceImpl(TravelRepository travelRepository, ElasticsearchRestTemplate elasticsearchRestTemplate, ThreadPoolExecutor dataSyncThreadPoolExecutor) {
        super(elasticsearchRestTemplate);
        this.travelRepository = travelRepository;
        this.elasticsearchRestTemplate = elasticsearchRestTemplate;
        this.dataSyncThreadPoolExecutor = dataSyncThreadPoolExecutor;
    }

    @Override
    public void save(TravelDTO travel) {
        TravelEs travelEs = new TravelEs();
        // 不推荐，性能差 => 推荐使用 MapStruct
        BeanUtils.copyProperties(travel, travelEs);
        log.info("[游记服务] 保存数据到 es: {}", travel.getId());
        travelRepository.save(travelEs);
    }

    @Override
    public void saveBatch(List<TravelDTO> strategies) {
        dataSyncThreadPoolExecutor.execute(() -> {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            List<TravelEs> collect = strategies.stream().map(travel -> {
                TravelEs travelEs = new TravelEs();
                // 不推荐，性能差 => 推荐使用 MapStruct
                BeanUtils.copyProperties(travel, travelEs);
                return travelEs;
            }).collect(Collectors.toList());
            travelRepository.saveAll(collect);
            stopWatch.stop();
            log.info("[游记服务] 批量保存数据：耗时：{}s, 数量：{}", stopWatch.getTotalTimeSeconds(), strategies.size());
        });
    }

    @Override
    public List<TravelEs> findListByDestName(String destName) {
        NativeSearchQuery query = new NativeSearchQueryBuilder().withQuery(QueryBuilders.matchPhraseQuery("destName", destName)).build();
        SearchHits<TravelEs> search = elasticsearchRestTemplate.search(query, TravelEs.class);
        return search.getSearchHits().stream().map(SearchHit::getContent).collect(Collectors.toList());
    }
}

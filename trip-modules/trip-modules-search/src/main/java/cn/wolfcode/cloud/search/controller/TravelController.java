package cn.wolfcode.cloud.search.controller;

import cn.wolfcode.cloud.article.dto.TravelDTO;
import cn.wolfcode.cloud.core.util.R;
import cn.wolfcode.cloud.search.service.TravelService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/travels")
@RequiredArgsConstructor
public class TravelController {

    private final TravelService travelService;

    @PostMapping
    public R<?> save(@RequestBody TravelDTO travel) {
        travelService.save(travel);
        return R.ok();
    }

    @PostMapping("/batch")
    public R<?> batchSave(@RequestBody List<TravelDTO> strategies) {
        travelService.saveBatch(strategies);
        return R.ok();
    }
}

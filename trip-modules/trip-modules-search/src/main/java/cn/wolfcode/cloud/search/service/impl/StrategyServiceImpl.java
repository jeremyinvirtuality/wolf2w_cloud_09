package cn.wolfcode.cloud.search.service.impl;

import cn.wolfcode.cloud.article.dto.StrategyDTO;
import cn.wolfcode.cloud.search.domain.StrategyEs;
import cn.wolfcode.cloud.search.repository.StrategyRepository;
import cn.wolfcode.cloud.search.service.StrategyService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.BeanUtils;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

@Slf4j
@Service
public class StrategyServiceImpl extends SearchServiceImpl<StrategyEs> implements StrategyService {

    private final StrategyRepository strategyRepository;
    private final ElasticsearchRestTemplate elasticsearchRestTemplate;
    private final ThreadPoolExecutor dataSyncThreadPoolExecutor;

    public StrategyServiceImpl(ElasticsearchRestTemplate elasticsearchRestTemplate,
                               ThreadPoolExecutor dataSyncThreadPoolExecutor,
                               StrategyRepository strategyRepository) {
        super(elasticsearchRestTemplate);
        this.strategyRepository = strategyRepository;
        this.elasticsearchRestTemplate = elasticsearchRestTemplate;
        this.dataSyncThreadPoolExecutor = dataSyncThreadPoolExecutor;
    }

    @Override
    public void save(StrategyDTO strategy) {
        StrategyEs strategyEs = new StrategyEs();
        // 不推荐，性能差 => 推荐使用 MapStruct
        BeanUtils.copyProperties(strategy, strategyEs);
        log.info("[攻略服务] 保存数据到 es: {}", strategy.getId());
        strategyRepository.save(strategyEs);
    }

    @Override
    public void saveBatch(List<StrategyDTO> strategies) {
        dataSyncThreadPoolExecutor.execute(() -> {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            List<StrategyEs> collect = strategies.stream().map(strategy -> {
                StrategyEs strategyEs = new StrategyEs();
                // 不推荐，性能差 => 推荐使用 MapStruct
                BeanUtils.copyProperties(strategy, strategyEs);
                return strategyEs;
            }).collect(Collectors.toList());
            strategyRepository.saveAll(collect);
            stopWatch.stop();
            log.info("[攻略服务] 批量保存数据：耗时：{}s, 数量：{}", stopWatch.getTotalTimeSeconds(), strategies.size());
        });
    }

    @Override
    public List<StrategyEs> findListByDestName(String destName) {
        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchPhraseQuery("destName", destName)).build();
        SearchHits<StrategyEs> search = elasticsearchRestTemplate.search(query, StrategyEs.class);
        return search.getSearchHits().stream().map(SearchHit::getContent).collect(Collectors.toList());
    }

    @Override
    public void updateById(StrategyDTO data) {
        StrategyEs strategyEs = new StrategyEs();
        // 不推荐，性能差 => 推荐使用 MapStruct
        BeanUtils.copyProperties(data, strategyEs);
        strategyRepository.save(strategyEs);
    }

    @Override
    public void deleteById(Long id) {
        strategyRepository.deleteById(id);
    }
}

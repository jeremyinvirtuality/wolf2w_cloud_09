package cn.wolfcode.cloud.search.service.impl;

import cn.wolfcode.cloud.search.query.SearchQuery;
import cn.wolfcode.cloud.search.service.SearchService;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SearchServiceImpl<T> implements SearchService<T> {

    private final ElasticsearchRestTemplate elasticsearchRestTemplate;

    public SearchServiceImpl(ElasticsearchRestTemplate elasticsearchRestTemplate) {
        this.elasticsearchRestTemplate = elasticsearchRestTemplate;
    }

    @Override
    public Page<T> queryWithHighlight(Class<T> searchClass, SearchQuery qo, String... fields) {
        // 1. 构建多条件查询对象
        MultiMatchQueryBuilder queryBuilder = QueryBuilders.multiMatchQuery(qo.getKeyword(), fields);
        // 2. 构建高亮查询对象
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        for (String field : fields) {
            highlightBuilder.field(field);
        }
        highlightBuilder.preTags("<em style=\"color:red\">");
        highlightBuilder.postTags("</em>");
        highlightBuilder.requireFieldMatch(false);
        highlightBuilder.numOfFragments(0);
        highlightBuilder.fragmentSize(900000);
        // 3. 构建分页对象
        PageRequest request = PageRequest.of(qo.getCurrent() - 1, qo.getSize());
        // 4. 组合查询对象
        NativeSearchQuery query = new NativeSearchQueryBuilder().withQuery(queryBuilder).withHighlightBuilder(highlightBuilder).withPageable(request).build();
        // 5. 查询数据
        SearchHits<T> search = elasticsearchRestTemplate.search(query, searchClass);
        // 6. 封装高亮数据
        List<T> list = new ArrayList<>();
        for (SearchHit<T> hit : search) {
            T content = hit.getContent();

            // 获取所有高亮字段
            Map<String, List<String>> highlightFields = hit.getHighlightFields();
            for (Map.Entry<String, List<String>> entry : highlightFields.entrySet()) {
                List<String> value = entry.getValue();
                String highlight = StringUtils.join(value, "");
                String key = entry.getKey();

                try {
                    // 利用内省机制，判断 map 的 key 与对象的属性相同的，自动调用 set 方法设置进去
                    BeanInfo beanInfo = Introspector.getBeanInfo(content.getClass(), Object.class);
                    // 获取到对象中的所有属性描述对象
                    PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
                    for (PropertyDescriptor property : propertyDescriptors) {
                        String name = property.getName();
                        Method setter = property.getWriteMethod();
                        if (key.equals(name) && setter != null) {
                            setter.invoke(content, highlight);
                        }
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            list.add(content);
        }
        return new PageImpl<>(list, request, search.getTotalHits());
    }
}
package cn.wolfcode.cloud.search.service;

import cn.wolfcode.cloud.article.dto.StrategyDTO;
import cn.wolfcode.cloud.search.domain.StrategyEs;

import java.util.List;

public interface StrategyService extends SearchService<StrategyEs> {

    void save(StrategyDTO strategy);

    void saveBatch(List<StrategyDTO> strategies);

    List<StrategyEs> findListByDestName(String destName);

    void updateById(StrategyDTO data);

    void deleteById(Long id);
}

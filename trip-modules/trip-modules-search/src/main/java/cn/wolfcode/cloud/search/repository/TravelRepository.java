package cn.wolfcode.cloud.search.repository;

import cn.wolfcode.cloud.search.domain.TravelEs;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface TravelRepository extends ElasticsearchRepository<TravelEs, Long> {
}

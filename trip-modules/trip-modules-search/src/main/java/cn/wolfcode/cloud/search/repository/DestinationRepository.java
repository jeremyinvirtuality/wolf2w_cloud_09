package cn.wolfcode.cloud.search.repository;

import cn.wolfcode.cloud.search.domain.DestinationEs;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface DestinationRepository extends ElasticsearchRepository<DestinationEs, Long> {
}

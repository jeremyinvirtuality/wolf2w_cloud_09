package cn.wolfcode.cloud.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TripSearchServer {

    public static void main(String[] args) {
        SpringApplication.run(TripSearchServer.class, args);
    }
}

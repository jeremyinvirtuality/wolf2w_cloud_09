package cn.wolfcode.cloud.article.mapper;

import cn.wolfcode.cloud.article.domain.StrategyContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface StrategyContentMapper extends BaseMapper<StrategyContent> {
}

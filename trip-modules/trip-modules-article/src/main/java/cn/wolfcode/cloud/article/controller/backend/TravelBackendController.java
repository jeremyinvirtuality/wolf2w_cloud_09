package cn.wolfcode.cloud.article.controller.backend;

import cn.wolfcode.cloud.article.domain.Travel;
import cn.wolfcode.cloud.article.service.TravelService;
import cn.wolfcode.cloud.core.query.QueryObject;
import cn.wolfcode.cloud.core.util.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/backend/travels")
@RequiredArgsConstructor
public class TravelBackendController {

    private final TravelService strategyService;

    @GetMapping("/list")
    public R<Page<Travel>> list(QueryObject qo) {
        return R.ok(strategyService.queryPage(qo));
    }

    @GetMapping("/detail")
    public R<Travel> detailById(Long id) {
        return R.ok(strategyService.getDetailById(id));
    }

    @PostMapping("/deleteById")
    public R<?> deleteById(Long id) {
        strategyService.removeById(id);
        return R.ok();
    }
}

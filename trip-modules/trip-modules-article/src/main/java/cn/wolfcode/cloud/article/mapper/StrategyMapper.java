package cn.wolfcode.cloud.article.mapper;

import cn.wolfcode.cloud.article.domain.Strategy;
import cn.wolfcode.cloud.article.query.StrategyQuery;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StrategyMapper extends BaseMapper<Strategy> {
    List<Strategy> findPage(Page<Strategy> page, @Param("qo") StrategyQuery qo);
}

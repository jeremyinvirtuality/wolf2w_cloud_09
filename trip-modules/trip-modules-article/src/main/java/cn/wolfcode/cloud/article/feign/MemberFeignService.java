package cn.wolfcode.cloud.article.feign;

import cn.wolfcode.clcoud.domain.UserInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Set;

@FeignClient("trip-member")
public interface MemberFeignService {

    @GetMapping("/users/findByIdList")
    List<UserInfo> findByIdList(@RequestParam Set<Long> idList);

    @GetMapping("/users/findFavoritesByUserId")
    List<Long> findFavoritesByUserId(@RequestParam Long id);
}

package cn.wolfcode.cloud.article.mapper;

import cn.wolfcode.cloud.article.domain.StrategyRank;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface StrategyRankMapper extends BaseMapper<StrategyRank> {
}

package cn.wolfcode.cloud.article.service.impl;

import cn.wolfcode.cloud.article.domain.Strategy;
import cn.wolfcode.cloud.article.domain.StrategyRank;
import cn.wolfcode.cloud.article.service.StaticsService;
import cn.wolfcode.cloud.article.service.StrategyRankService;
import cn.wolfcode.cloud.article.service.StrategyService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StaticsServiceImpl implements StaticsService {

    private final StrategyService strategyService;
    private final StrategyRankService strategyRankService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void rankingByType(Integer type) {
        // 先删除之前的排行数据
        strategyRankService.remove(new LambdaQueryWrapper<StrategyRank>()
                .eq(StrategyRank::getType, type));

        QueryWrapper<Strategy> wrapper = new QueryWrapper<>();
        // 类型不等于热门，就是国内国外
        if (!type.equals(StrategyRank.TYPE_HOT)) {
            wrapper.select("id strategyId", "title strategyTitle",
                    "dest_id destId", "dest_name destName", "thumbsupnum + favornum as statisnum");
            // 拼接国内|国外查询条件
            int isAbroad = type == StrategyRank.TYPE_ABROAD ? 1 : 0;
            wrapper.lambda().eq(Strategy::getIsabroad, isAbroad);
            // 拼接排序条件
            wrapper.orderByDesc("thumbsupnum + favornum");
        } else {
            wrapper.select("id strategyId", "title strategyTitle",
                    "dest_id destId", "dest_name destName", "viewnum + replynum as statisnum");
            wrapper.orderByDesc("viewnum + replynum");
        }
        wrapper.last(" LIMIT 10 ");

        // 1. 查询统计数据
        List<Map<String, Object>> maps = strategyService.listMaps(wrapper);
        Date now = new Date();
        // 2. 转换成攻略排行数据对象
        List<StrategyRank> collect = maps.stream()
                .map(map -> {
                    StrategyRank rank = new StrategyRank();
                    rank.setType(type);
                    rank.setDestName((String) map.get("destName"));
                    rank.setDestId((Long) map.get("destId"));
                    rank.setStatisnum((Long) map.get("statisnum"));
                    rank.setStatisTime(now);
                    rank.setStrategyId((Long) map.get("strategyId"));
                    rank.setStrategyTitle((String) map.get("strategyTitle"));
                    return rank;
                })
                .collect(Collectors.toList());
        // 3. 批量保存统计排行数据
        strategyRankService.saveBatch(collect);
    }
}

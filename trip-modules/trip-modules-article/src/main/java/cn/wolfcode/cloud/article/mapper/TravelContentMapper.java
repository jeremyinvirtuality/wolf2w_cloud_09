package cn.wolfcode.cloud.article.mapper;

import cn.wolfcode.cloud.article.domain.TravelContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TravelContentMapper extends BaseMapper<TravelContent> {
}

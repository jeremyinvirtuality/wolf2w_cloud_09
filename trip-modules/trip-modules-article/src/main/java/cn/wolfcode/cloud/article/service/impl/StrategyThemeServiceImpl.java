package cn.wolfcode.cloud.article.service.impl;

import cn.wolfcode.cloud.article.domain.StrategyTheme;
import cn.wolfcode.cloud.article.mapper.StrategyThemeMapper;
import cn.wolfcode.cloud.article.service.StrategyThemeService;
import cn.wolfcode.cloud.article.vo.StrategyCondition;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StrategyThemeServiceImpl extends ServiceImpl<StrategyThemeMapper, StrategyTheme> implements StrategyThemeService {
    @Override
    public List<StrategyCondition> findConditions() {
        return baseMapper.selectConditions();
    }
}

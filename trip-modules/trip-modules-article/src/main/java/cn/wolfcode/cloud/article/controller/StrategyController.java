package cn.wolfcode.cloud.article.controller;

import cn.wolfcode.cloud.article.domain.Strategy;
import cn.wolfcode.cloud.article.domain.StrategyCatalog;
import cn.wolfcode.cloud.article.domain.StrategyContent;
import cn.wolfcode.cloud.article.query.StrategyQuery;
import cn.wolfcode.cloud.article.service.StrategyService;
import cn.wolfcode.cloud.article.vo.SimpleThemeVo;
import cn.wolfcode.cloud.auth.anno.RequestUser;
import cn.wolfcode.cloud.auth.vo.LoginUser;
import cn.wolfcode.cloud.core.util.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/strategies")
@RequiredArgsConstructor
public class StrategyController {

    private final StrategyService strategyService;

    @GetMapping("/query")
    public R<Page<Strategy>> queryPage(StrategyQuery qo) {
        Page<Strategy> page = strategyService.findPage(qo);
        return R.ok(page);
    }

    @GetMapping("/detail")
    public R<Strategy> detailById(Long id) {
        Strategy strategy = strategyService.getDetailById(id);
        // AssertUtils.equals(strategy.getState(), Strategy.STATE_NORMAL, "数据异常");
        if (Strategy.STATE_NORMAL != strategy.getState()) {
            strategy = null;
        }
        return R.ok(strategy);
    }

    @PostMapping("/thumb")
    public R<Boolean> thumb(Long strategyId, @RequestUser LoginUser user) {
        return R.ok(strategyService.toThumb(strategyId, user.getId()));
    }

    @GetMapping("/destinations/themes")
    public R<List<SimpleThemeVo>> findThemesByDestId(Long destId) {
        List<SimpleThemeVo> list = strategyService.findThemesByDestId(destId);
        return R.ok(list);
    }

    @GetMapping("/stat/data")
    public R<Map<Object, Object>> findStatData(Long id) {
        return R.ok(strategyService.findStatData(id));
    }

    @GetMapping("/viewnumTop3")
    public R<List<Strategy>> viewnumTop3(Long destId) {
        List<Strategy> list = strategyService.viewnumTop3(destId);
        return R.ok(list);
    }

    @GetMapping("/groups")
    public R<List<StrategyCatalog>> groupList(Long destId) {
        List<StrategyCatalog> list = strategyService.queryCatalogGroupList(destId);
        return R.ok(list);
    }

    @GetMapping("/content")
    public R<StrategyContent> content(Long id) {
        StrategyContent content = strategyService.getContentById(id);
        return R.ok(content);
    }
}

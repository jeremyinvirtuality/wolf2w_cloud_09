package cn.wolfcode.cloud.article.mapper;

import cn.wolfcode.cloud.article.domain.StrategyTheme;
import cn.wolfcode.cloud.article.vo.StrategyCondition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

public interface StrategyThemeMapper extends BaseMapper<StrategyTheme> {
    List<StrategyCondition> selectConditions();
}

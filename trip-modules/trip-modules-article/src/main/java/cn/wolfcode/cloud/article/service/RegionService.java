package cn.wolfcode.cloud.article.service;

import cn.wolfcode.cloud.article.domain.Region;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface RegionService extends IService<Region> {
    List<Region> findHotList();
}

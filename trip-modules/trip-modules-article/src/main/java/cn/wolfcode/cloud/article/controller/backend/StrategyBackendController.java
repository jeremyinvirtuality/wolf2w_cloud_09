package cn.wolfcode.cloud.article.controller.backend;

import cn.wolfcode.cloud.article.domain.Strategy;
import cn.wolfcode.cloud.article.service.StrategyService;
import cn.wolfcode.cloud.article.utils.OssUtil;
import cn.wolfcode.cloud.core.query.QueryObject;
import cn.wolfcode.cloud.core.util.R;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/backend/strategies")
@RequiredArgsConstructor
public class StrategyBackendController {

    private final StrategyService strategyService;

    @GetMapping("/list")
    public R<Page<Strategy>> list(QueryObject qo) {
        return R.ok(strategyService.queryPage(qo));
    }

    @GetMapping("/detail")
    public R<Strategy> detailById(Long id) {
        return R.ok(strategyService.getDetailById(id));
    }

    @PostMapping("/save")
    public R<?> save(Strategy strategy) {
        strategyService.save(strategy);
        return R.ok();
    }

    @PostMapping("/updateById")
    public R<?> updateById(Strategy strategy) {
        strategyService.updateById(strategy);
        return R.ok();
    }

    @PostMapping("/uploadImg")
    public JSONObject uploadImg(MultipartFile upload) {
        JSONObject result = new JSONObject();
        try {
            String url = OssUtil.upload("wolf2w/images", upload);

            result.put("uploaded", 1);
            result.put("fileName", upload.getOriginalFilename());
            result.put("url", url);
        } catch (Exception e) {
            e.printStackTrace();
            result.put("uploaded", 0);
            JSONObject error = new JSONObject();
            error.put("message", e.getMessage());
            result.put("error", error);
        }

        return result;
    }

    @PostMapping("/deleteById")
    public R<?> deleteById(Long id) {
        strategyService.removeById(id);
        return R.ok();
    }
}

package cn.wolfcode.cloud.article.service.impl;

import cn.wolfcode.clcoud.domain.UserInfo;
import cn.wolfcode.cloud.article.domain.Travel;
import cn.wolfcode.cloud.article.domain.TravelContent;
import cn.wolfcode.cloud.article.feign.MemberFeignService;
import cn.wolfcode.cloud.article.mapper.TravelContentMapper;
import cn.wolfcode.cloud.article.mapper.TravelMapper;
import cn.wolfcode.cloud.article.query.TravelQuery;
import cn.wolfcode.cloud.article.service.TravelService;
import cn.wolfcode.cloud.auth.utils.AuthUtils;
import cn.wolfcode.cloud.auth.vo.LoginUser;
import cn.wolfcode.cloud.core.query.QueryObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TravelServiceImpl extends ServiceImpl<TravelMapper, Travel> implements TravelService {
    private final TravelContentMapper strategyContentMapper;
    private final MemberFeignService memberFeignService;
    private final AuthUtils authUtils;

    @Override
    public Page<Travel> queryPage(QueryObject qo) {
        LambdaQueryWrapper<Travel> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(!StringUtils.isEmpty(qo.getKeyword()), Travel::getTitle, qo.getKeyword());
        return super.page(new Page<>(qo.getCurrent(), qo.getSize()), wrapper);
    }

    @Override
    public Travel getDetailById(Long id) {
        Travel strategy = getById(id);
        if (strategy != null) {
            TravelContent content = strategyContentMapper.selectById(id);
            strategy.setContent(content);
            List<UserInfo> userInfos = memberFeignService.findByIdList(Collections.singleton(strategy.getAuthorId()));
            if (userInfos != null && userInfos.size() > 0) {
                strategy.setAuthor(userInfos.get(0));
            }
        }
        return strategy;
    }

    @Override
    public TravelContent getContentById(Long id) {
        return strategyContentMapper.selectById(id);
    }

    @Override
    public List<Travel> viewnumTop3(Long destId) {
        return list(new LambdaQueryWrapper<Travel>().eq(Travel::getDestId, destId).eq(Travel::getState, Travel.STATE_NORMAL).orderByDesc(Travel::getViewnum).last("limit 3"));
    }

    @Override
    public Page<Travel> findPage(TravelQuery qo) {
        QueryWrapper<Travel> wrapper = new QueryWrapper<>();
        // 拼接条件
        wrapper.lambda().eq(qo.getDestId() != null, Travel::getDestId, qo.getDestId());
        // 游客：   ispublic=1 and state=2
        // 游客查询条件 (ispublic=1 and state=2)
        wrapper.nested(w -> {
                    w.lambda().eq(Travel::getIspublic, Travel.ISPUBLIC_YES)
                            .eq(Travel::getState, Travel.STATE_RELEASE);
                    if (qo.getDayType() != null) {
                        w.lambda().between(Travel::getDay, qo.getDayType().getMin(), qo.getDayType().getMax());
                    }
                    if (qo.getTravelTimeType() != null) {
                        w.between("MONTH(travel_time)", qo.getTravelTimeType().getMin(), qo.getTravelTimeType().getMax());
                    }
                    if (qo.getConsumeType() != null) {
                        w.lambda().between(Travel::getAvgConsume, qo.getConsumeType().getMin(), qo.getConsumeType().getMax());
                    }
                }
        );
        // 用户查询条件（自己的所有（私有、草稿、已拒绝） AND 别人的公开和审核通过的数据）
        LoginUser loginUser = authUtils.getLoginUser();
        if (loginUser != null) {
            // 当前用户登录了
            // 已登录： OR autor_id = #{user.id} and dest_id = #{destId}
            wrapper.or(w -> {
                w.lambda().eq(Travel::getAuthorId, loginUser.getId())
                        .eq(qo.getDestId() != null, Travel::getDestId, qo.getDestId());
                if (qo.getDayType() != null) {
                    w.lambda().between(Travel::getDay, qo.getDayType().getMin(), qo.getDayType().getMax());
                }
                if (qo.getTravelTimeType() != null) {
                    w.between("MONTH(travel_time)", qo.getTravelTimeType().getMin(), qo.getTravelTimeType().getMax());
                }
                if (qo.getConsumeType() != null) {
                    w.lambda().between(Travel::getAvgConsume, qo.getConsumeType().getMin(), qo.getConsumeType().getMax());
                }
            });
        }
        // 排序
        wrapper.lambda().orderByDesc(qo.getOrderBy() != null, qo.getOrderBy());

        Page<Travel> page = page(new Page<>(qo.getCurrent(), qo.getSize()), wrapper);
        List<Travel> records = page.getRecords();

        // 将游记列表转换为作者 id 列表
        Set<Long> authorIdList = records.stream().map(Travel::getAuthorId).collect(Collectors.toSet());
        if (authorIdList.size() > 0) {
            // 远程基于作者id列表查询作者列表
            List<UserInfo> userInfos = memberFeignService.findByIdList(authorIdList);
            Map<Long, UserInfo> userInfoMap = new HashMap<>();
            for (UserInfo userInfo : userInfos) {
                userInfoMap.put(userInfo.getId(), userInfo);
            }
            for (Travel travel : records) {
                travel.setAuthor(userInfoMap.get(travel.getAuthorId()));
            }
        }

        return page;
    }
}

package cn.wolfcode.cloud.article.service;

import cn.wolfcode.cloud.article.domain.Strategy;
import cn.wolfcode.cloud.article.domain.StrategyCatalog;
import cn.wolfcode.cloud.article.domain.StrategyContent;
import cn.wolfcode.cloud.article.query.StrategyQuery;
import cn.wolfcode.cloud.article.vo.SimpleThemeVo;
import cn.wolfcode.cloud.core.query.QueryObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

public interface StrategyService extends IService<Strategy> {

    Page<Strategy> queryPage(QueryObject qo);

    Strategy getDetailById(Long id);

    List<StrategyCatalog> queryCatalogGroupList(Long destId);

    StrategyContent getContentById(Long id);

    List<Strategy> viewnumTop3(Long destId);

    List<SimpleThemeVo> findThemesByDestId(Long destId);

    Page<Strategy> findPage(StrategyQuery qo);

    Boolean toThumb(Long strategyId, Long id);

    Map<Object, Object> findStatData(Long id);
}

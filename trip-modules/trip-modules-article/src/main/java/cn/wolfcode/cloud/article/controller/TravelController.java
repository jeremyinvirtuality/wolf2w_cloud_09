package cn.wolfcode.cloud.article.controller;

import cn.wolfcode.cloud.article.domain.Travel;
import cn.wolfcode.cloud.article.domain.TravelContent;
import cn.wolfcode.cloud.article.query.TravelQuery;
import cn.wolfcode.cloud.article.service.TravelService;
import cn.wolfcode.cloud.core.util.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/travels")
@RequiredArgsConstructor
public class TravelController {

    private final TravelService strategyService;

    @GetMapping("/query")
    public R<Page<Travel>> queryPage(TravelQuery qo) {
        Page<Travel> page = strategyService.findPage(qo);
        return R.ok(page);
    }

    @GetMapping("/detail")
    public R<Travel> detailById(Long id) {
        Travel strategy = strategyService.getDetailById(id);
        return R.ok(strategy);
    }

    @GetMapping("/viewnumTop3")
    public R<List<Travel>> viewnumTop3(Long destId) {
        List<Travel> list = strategyService.viewnumTop3(destId);
        return R.ok(list);
    }

    @GetMapping("/content")
    public R<TravelContent> content(Long id) {
        TravelContent content = strategyService.getContentById(id);
        return R.ok(content);
    }
}

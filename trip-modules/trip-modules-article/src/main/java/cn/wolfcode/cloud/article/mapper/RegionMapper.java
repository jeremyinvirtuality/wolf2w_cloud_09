package cn.wolfcode.cloud.article.mapper;

import cn.wolfcode.cloud.article.domain.Region;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface RegionMapper extends BaseMapper<Region> {
}

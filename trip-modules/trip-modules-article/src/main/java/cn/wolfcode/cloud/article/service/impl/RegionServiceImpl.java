package cn.wolfcode.cloud.article.service.impl;

import cn.wolfcode.cloud.article.domain.Region;
import cn.wolfcode.cloud.article.mapper.RegionMapper;
import cn.wolfcode.cloud.article.service.RegionService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements RegionService {
    @Override
    public List<Region> findHotList() {
        LambdaQueryWrapper<Region> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Region::getIshot, Region.STATE_HOT);
        wrapper.orderByAsc(Region::getSeq);

        return list(wrapper);
    }
}

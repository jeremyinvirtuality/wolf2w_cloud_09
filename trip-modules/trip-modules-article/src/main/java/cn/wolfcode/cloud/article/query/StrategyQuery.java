package cn.wolfcode.cloud.article.query;

import cn.wolfcode.cloud.article.domain.Strategy;
import cn.wolfcode.cloud.article.vo.StrategyCondition;
import cn.wolfcode.cloud.core.query.QueryObject;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StrategyQuery extends QueryObject {

    private String orderBy = "viewnum";
    private Long refid;
    private Integer type;
    private Long destId;
    private Long themeId;

    public void setType(Integer type) {
        if (type != null && type > 0) {
            // 判断当前查询类型是否是主题
            if (type == StrategyCondition.TYPE_THEME) {
                // 如果是主题，就按主题 id 查询
                this.themeId = this.refid;
            } else {
                // 否则按照目的地 id 查询
                this.destId = this.refid;
            }
        }
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}

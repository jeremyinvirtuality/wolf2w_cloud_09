package cn.wolfcode.cloud.article.feign;

import cn.wolfcode.cloud.article.domain.Destination;
import cn.wolfcode.cloud.article.domain.Strategy;
import cn.wolfcode.cloud.article.domain.Travel;
import cn.wolfcode.cloud.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("trip-search")
public interface SearchFeignService {

    @PostMapping("/strategies")
    R<?> save(@RequestBody Strategy strategy);

    @PostMapping("/strategies/batch")
    R<?> batchSaveStrategy(@RequestBody List<Strategy> strategies);

    @PostMapping("/destinations/batch")
    R<?> batchSaveDestination(@RequestBody List<Destination> destinations);

    @PostMapping("/travels/batch")
    R<?> batchSaveTravel(@RequestBody List<Travel> travels);

    @PostMapping("/{type}/data/sync/{operate}")
    R<?> dataSync(@PathVariable("type") String type, @PathVariable("operate") String operate, @RequestBody Object data);
}

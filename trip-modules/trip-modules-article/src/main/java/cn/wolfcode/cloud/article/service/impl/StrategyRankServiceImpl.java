package cn.wolfcode.cloud.article.service.impl;

import cn.wolfcode.cloud.article.domain.StrategyRank;
import cn.wolfcode.cloud.article.mapper.StrategyRankMapper;
import cn.wolfcode.cloud.article.service.StrategyRankService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class StrategyRankServiceImpl extends ServiceImpl<StrategyRankMapper, StrategyRank> implements StrategyRankService {
    @Override
    public List<StrategyRank> findByType(int type) {
        String now = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.now());
        return list(
                new QueryWrapper<StrategyRank>()
                        .eq("DATE_FORMAT(statis_time, '%Y-%m-%d')", now)
                        .lambda().eq(StrategyRank::getType, type)
        );
    }
}

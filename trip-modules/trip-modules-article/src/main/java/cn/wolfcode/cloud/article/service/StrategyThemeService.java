package cn.wolfcode.cloud.article.service;

import cn.wolfcode.cloud.article.domain.StrategyTheme;
import cn.wolfcode.cloud.article.vo.StrategyCondition;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface StrategyThemeService extends IService<StrategyTheme> {

    List<StrategyCondition> findConditions();
}

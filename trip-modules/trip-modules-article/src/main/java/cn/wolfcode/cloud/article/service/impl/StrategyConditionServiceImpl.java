package cn.wolfcode.cloud.article.service.impl;

import cn.wolfcode.cloud.article.service.DestinationService;
import cn.wolfcode.cloud.article.service.StrategyConditionService;
import cn.wolfcode.cloud.article.service.StrategyThemeService;
import cn.wolfcode.cloud.article.vo.StrategyCondition;
import cn.wolfcode.cloud.redis.key.RedisKeyEnum;
import cn.wolfcode.cloud.redis.service.RedisService;
import com.alibaba.fastjson2.JSONObject;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.*;

@Service
@RequiredArgsConstructor
public class StrategyConditionServiceImpl implements StrategyConditionService {

    private final DestinationService destinationService;
    private final StrategyThemeService strategyThemeService;
    private final RedisService redisService;

    @Override
    public List<StrategyCondition> findByDestType(int type) {
        type = type == 1 ? 1 : 0;
        return destinationService.findConditionsByAbroad(type);
    }

    @Override
    public List<StrategyCondition> findByThemes() {
        return strategyThemeService.findConditions();
    }

    @Override
    public JSONObject findFromRedis() {
        long beigin = System.currentTimeMillis();
        // 0. 创建一个 Map，key=字符串id，value=条件对象
        Map<String, StrategyCondition> conditionMap = new HashMap<>();
        // 1. 从 redis 中获取整个 hash 对象
        Map<Object, Object> hash = redisService.hgetAll(RedisKeyEnum.STRATEGY_CONDITIONS_DESTINATIONS);
        // 将 redis 查到的 map 转换为 condition 对象并存入 destConditionMap
        handleMapToCondition(conditionMap, hash);
        // 7. 将 Map 转换为条件集合，并将不同类型的条件，存入不同的集合中
        List<StrategyCondition> abroadCondition = new ArrayList<>();
        List<StrategyCondition> chinaCondition = new ArrayList<>();
        for (StrategyCondition condition : conditionMap.values()) {
            if (condition.getType() == StrategyCondition.TYPE_ABROAD) {
                abroadCondition.add(condition);
            } else if (condition.getType() == StrategyCondition.TYPE_CHINA) {
                chinaCondition.add(condition);
            }
        }
        // 清空 map 之前的数据
        conditionMap.clear();
        hash = redisService.hgetAll(RedisKeyEnum.STRATEGY_CONDITIONS_THEMES);
        // 将 redis 查到的 map 转换为 condition 对象并存入 destConditionMap
        handleMapToCondition(conditionMap, hash);
        Collection<StrategyCondition> themeCondition = conditionMap.values();

        System.out.println("redis 查询条件数据耗时：" + (System.currentTimeMillis() - beigin));
        // 8. 返回数据
        return JSONObject.of("abroadCondition", abroadCondition,
                "chinaCondition", chinaCondition, "themeCondition", themeCondition);
    }

    private static void handleMapToCondition(Map<String, StrategyCondition> conditionMap, Map<Object, Object> hash) {
        // 2. 遍历 hash 对象，得到 key/value
        for (Map.Entry<Object, Object> entry : hash.entrySet()) {
            // 3. 使用 : 对 key 进行分割
            String[] arr = entry.getKey().toString().split(":");
            // 4. 获取分割后的第二个值（id），从 0 步的Map中查询是否有条件对象
            StrategyCondition condition = conditionMap.get(arr[1]);
            // 5. 如果没有，创建一个新的对象，并将当前值设置进去，存入 map 中
            if (condition == null) {
                condition = new StrategyCondition();
                conditionMap.put(arr[1], condition);
            }
            // 6. 如果有，基于分割的第一个值（属性名），将值设置到对象中
            setField(arr[0], condition, entry.getValue());
        }
    }

    private static void setField(String filedName, StrategyCondition condition, Object value) {
        Field field = ReflectionUtils.findField(StrategyCondition.class, filedName);
        ReflectionUtils.makeAccessible(field);
        if (field.getType() == Long.class) {
            ReflectionUtils.setField(field, condition, Long.valueOf(value + ""));
        } else if (field.getType() == Integer.class) {
            ReflectionUtils.setField(field, condition, Integer.valueOf(value + ""));
        } else {
            ReflectionUtils.setField(field, condition, value);
        }
    }
}

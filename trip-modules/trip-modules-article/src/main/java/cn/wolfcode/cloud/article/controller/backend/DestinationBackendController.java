package cn.wolfcode.cloud.article.controller.backend;

import cn.wolfcode.cloud.article.domain.Destination;
import cn.wolfcode.cloud.article.query.DestinationQuery;
import cn.wolfcode.cloud.article.service.DestinationService;
import cn.wolfcode.cloud.core.util.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@ApiIgnore
@RestController
@RequestMapping("/backend/destinations")
@RequiredArgsConstructor
public class DestinationBackendController {

    private final DestinationService destinationService;

    @GetMapping("/list")
    public R<Page<Destination>> list(DestinationQuery qo) {
        return R.ok(destinationService.queryPage(qo));
    }

    @GetMapping("/all")
    public R<List<Destination>> listAll() {
        return R.ok(destinationService.list());
    }

    @GetMapping("/detail")
    public R<Destination> detailById(Long id) {
        return R.ok(destinationService.getById(id));
    }

    @GetMapping("/toasts")
    public R<List<Destination>> queryToasts(Long destId) {
        return R.ok(destinationService.queryToasts(destId));
    }

    @PostMapping("/save")
    public R<?> save(Destination destination) {
        destinationService.save(destination);
        return R.ok();
    }

    @PostMapping("/updateById")
    public R<?> updateById(Destination destination) {
        destinationService.updateById(destination);
        return R.ok();
    }

    @PostMapping("/deleteById")
    public R<?> deleteById(Long id) {
        destinationService.removeById(id);
        return R.ok();
    }
}

package cn.wolfcode.cloud.article.service;

import cn.wolfcode.cloud.article.domain.Travel;
import cn.wolfcode.cloud.article.domain.TravelContent;
import cn.wolfcode.cloud.article.query.TravelQuery;
import cn.wolfcode.cloud.core.query.QueryObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface TravelService extends IService<Travel> {

    Page<Travel> queryPage(QueryObject qo);

    Travel getDetailById(Long id);

    TravelContent getContentById(Long id);

    List<Travel> viewnumTop3(Long destId);

    Page<Travel> findPage(TravelQuery qo);
}

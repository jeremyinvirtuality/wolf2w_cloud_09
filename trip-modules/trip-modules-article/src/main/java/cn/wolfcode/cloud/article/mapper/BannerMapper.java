package cn.wolfcode.cloud.article.mapper;

import cn.wolfcode.cloud.article.domain.Banner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface BannerMapper extends BaseMapper<Banner> {
}

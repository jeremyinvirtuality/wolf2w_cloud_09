package cn.wolfcode.cloud.article.listener;

import cn.wolfcode.cloud.article.enums.ElasticsearchDataSyncEnum;
import cn.wolfcode.cloud.article.enums.OperateTypeEnum;
import cn.wolfcode.cloud.article.events.ElasticsearchDataSyncEvent;
import cn.wolfcode.cloud.article.feign.SearchFeignService;
import cn.wolfcode.cloud.core.util.R;
import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ElasticsearchDataSyncListener implements ApplicationListener<ElasticsearchDataSyncEvent> {

    private final SearchFeignService searchFeignService;

    @Override
    public void onApplicationEvent(ElasticsearchDataSyncEvent event) {
        log.info("[ES数据同步] 收到数据同步事件：type={}, operate={}, source={}", event.getDataType(), event.getOperateType(), event.getSource());
        // 针对三种不同类型的数据做处理
        ElasticsearchDataSyncEnum dataType = event.getDataType();
        OperateTypeEnum operateType = event.getOperateType();
        R<?> result = searchFeignService.dataSync(dataType.getName(), operateType.getName(), event.getSource());
        log.info("[ES数据同步] 数据同步完成，收到响应结果：{}", JSON.toJSONString(result));
    }
}

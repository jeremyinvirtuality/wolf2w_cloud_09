package cn.wolfcode.cloud.article.service.impl;

import cn.wolfcode.cloud.article.domain.*;
import cn.wolfcode.cloud.article.enums.OperateTypeEnum;
import cn.wolfcode.cloud.article.events.StrategyDataSyncEvent;
import cn.wolfcode.cloud.article.events.StrategyEvent;
import cn.wolfcode.cloud.article.feign.MemberFeignService;
import cn.wolfcode.cloud.article.mapper.StrategyContentMapper;
import cn.wolfcode.cloud.article.mapper.StrategyMapper;
import cn.wolfcode.cloud.article.query.StrategyQuery;
import cn.wolfcode.cloud.article.service.DestinationService;
import cn.wolfcode.cloud.article.service.StrategyCatalogService;
import cn.wolfcode.cloud.article.service.StrategyService;
import cn.wolfcode.cloud.article.service.StrategyThemeService;
import cn.wolfcode.cloud.article.utils.OssUtil;
import cn.wolfcode.cloud.article.vo.SimpleThemeVo;
import cn.wolfcode.cloud.auth.utils.AuthUtils;
import cn.wolfcode.cloud.auth.vo.LoginUser;
import cn.wolfcode.cloud.core.query.QueryObject;
import cn.wolfcode.cloud.core.util.DateUtils;
import cn.wolfcode.cloud.redis.events.StrategyStatEvent;
import cn.wolfcode.cloud.redis.key.RedisKeyEnum;
import cn.wolfcode.cloud.redis.service.RedisService;
import cn.wolfcode.cloud.redis.utils.SpringContextUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StrategyServiceImpl extends ServiceImpl<StrategyMapper, Strategy> implements StrategyService {

    private final StrategyThemeService strategyThemeService;
    private final StrategyCatalogService strategyCatalogService;
    private final DestinationService destinationService;
    private final StrategyContentMapper strategyContentMapper;
    private final TransactionTemplate transactionTemplate;
    private final ApplicationContext ctx;
    private final RedisService redisService;
    private final AuthUtils authUtils;
    @Lazy
    @Autowired
    private MemberFeignService memberFeignService;

    @Override
    public Page<Strategy> queryPage(QueryObject qo) {
        LambdaQueryWrapper<Strategy> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(!StringUtils.isEmpty(qo.getKeyword()), Strategy::getTitle, qo.getKeyword());
        return super.page(new Page<>(qo.getCurrent(), qo.getSize()), wrapper);
    }

    @Override
    public Strategy getDetailById(Long id) {
        Strategy strategy = getById(id);
        if (strategy != null) {
            StrategyContent content = strategyContentMapper.selectById(id);
            strategy.setContent(content);

            // 如果当前用户登录了，查询是否已收藏
            LoginUser user = authUtils.getLoginUser();
            if (user != null) {
                // 调用用户 api 获取用户收藏的文章列表
                List<Long> favorites = memberFeignService.findFavoritesByUserId(user.getId());
                strategy.setFavorite(favorites.contains(id));
            }

            // 攻略阅读数+1
            SpringContextUtil.publishEvent(new StrategyStatEvent(id, Strategy.HASH_VIEW, 1));
            // 获取攻略统计数据
            strategy.syncStatData(this.findStatData(id));
        }
        return strategy;
    }

    @Override
    public List<StrategyCatalog> queryCatalogGroupList(Long destId) {
        List<StrategyCatalog> catalogList = strategyCatalogService.findByDestId(destId);
        List<CompletableFuture<Void>> futureList = new ArrayList<>(catalogList.size());
        for (StrategyCatalog catalog : catalogList) {
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                List<Strategy> strategies = list(new LambdaQueryWrapper<Strategy>().eq(Strategy::getCatalogId, catalog.getId()).eq(Strategy::getState, Strategy.STATE_NORMAL).orderByDesc(Strategy::getViewnum).last("limit 5"));
                catalog.setStrategies(strategies);
            });
            futureList.add(future);
        }

        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[]{})).join();
        return catalogList;
    }

    @Override
    public StrategyContent getContentById(Long id) {
        return strategyContentMapper.selectById(id);
    }

    @Override
    public List<Strategy> viewnumTop3(Long destId) {
        return list(new LambdaQueryWrapper<Strategy>().eq(Strategy::getDestId, destId).eq(Strategy::getState, Strategy.STATE_NORMAL).orderByDesc(Strategy::getViewnum).last("limit 3"));
    }

    @Override
    public List<SimpleThemeVo> findThemesByDestId(Long destId) {
        List<Strategy> strategies = list(new LambdaQueryWrapper<Strategy>().eq(Strategy::getDestId, destId).eq(Strategy::getState, Strategy.STATE_NORMAL));

        return strategies.stream().map(strategy -> {
            SimpleThemeVo vo = new SimpleThemeVo();
            vo.setId(strategy.getThemeId());
            vo.setName(strategy.getThemeName());
            return vo;
        }).distinct().collect(Collectors.toList());
    }

    @Override
    public Page<Strategy> findPage(StrategyQuery qo) {
        Page<Strategy> page = new Page<>(qo.getCurrent(), qo.getSize());
        List<Strategy> list = baseMapper.findPage(page, qo);
        page.setRecords(list);
        return page;
    }

    @Override
    public Boolean toThumb(Long strategyId, Long id) {
        // 1. 保存 redis key 是否成功
        RedisKeyEnum prefix = RedisKeyEnum.STRATEGY_THUMB_USERS_HASH;
        prefix.setTimeout(DateUtils.getBetweenSecondsOfNowToLastSeconds());
        boolean result = redisService.hsetnx(prefix, id + "", "1", strategyId + "");
        // 2. 如果保存成功，数量+1
        if (result) {
            SpringContextUtil.publishEvent(new StrategyStatEvent(strategyId, Strategy.HASH_THUMBSUP, 1));
        }
        // 3. 根据保存成功/失败结果进行提示
        return result;
    }

    @Override
    public Map<Object, Object> findStatData(Long id) {
        return redisService.hgetAll(RedisKeyEnum.STRATEGY_STATICS_HASH, id + "");
    }

    @Override
    public boolean save(final Strategy entity) {
        long totalBegin = System.currentTimeMillis();
        CompletableFuture<Void> uploadCoverFuture = CompletableFuture.runAsync(() -> {
            // 1. 将 cover_url 的数据上传到阿里云
            if (!StringUtils.isEmpty(entity.getCoverUrl())) {
                long begin = System.currentTimeMillis();
                String url = OssUtil.uploadImgByBase64("wolf2w/images", UUID.randomUUID() + ".jpg", entity.getCoverUrl());
                entity.setCoverUrl(url);
                System.out.println("上传耗时：" + (System.currentTimeMillis() - begin));
            }
        });
        CompletableFuture<Void> syncDataFuture = CompletableFuture.runAsync(() -> {
            // 2. 补充分类、主题、目的地的名称
            this.setThemeCatalogDestInfo(entity);
            // 3. 设置创建时间
            entity.setCreateTime(new Date());
            // 5. 重新设置 state 为正常状态, 重新覆盖文章各种统计数据
            this.overrideProp(entity);
        });

        CompletableFuture<Void> future = CompletableFuture.allOf(uploadCoverFuture, syncDataFuture);
        future.join(); // 开始执行所有任务，并且等待所有任务执行完毕，主线程在此阻塞，直至所有任务执行完毕
        System.out.println("事务前总耗时：" + (System.currentTimeMillis() - totalBegin));

        // 7. 保存攻略对象，得到攻略自增 id
        // 8. 将攻略自增 id 设置到攻略内容对象上
        // 9. 保存攻略内容对象
        return Boolean.TRUE.equals(transactionTemplate.execute(status -> {
            // 7. 保存攻略对象，得到攻略自增 id
            boolean save = super.save(entity);
            // 8. 将攻略自增 id 设置到攻略内容对象上
            StrategyContent content = entity.getContent();
            content.setId(entity.getId());
            // 9. 保存攻略内容对象
            int insert = strategyContentMapper.insert(content);
            boolean result = save && insert > 0;
            if (result) {
                // 10. 发布新增攻略事件
                ctx.publishEvent(new StrategyEvent(OperateTypeEnum.INSERT, entity));
                SpringContextUtil.publishEvent(new StrategyDataSyncEvent(OperateTypeEnum.INSERT, entity));
            }
            return result;
        }));
    }

    @Override
    public boolean removeById(Serializable id) {
        // 删除对象前，查询当前对象
        Strategy strategy = getById(id);
        if (strategy == null) {
            return false;
        }

        boolean result = super.removeById(id);
        if (result) {
            // 10. 发布新增攻略事件
            ctx.publishEvent(new StrategyEvent(OperateTypeEnum.DELETE, strategy));
            SpringContextUtil.publishEvent(new StrategyDataSyncEvent(OperateTypeEnum.DELETE, id));
        }
        return result;
    }

    @Override
    public boolean updateById(final Strategy entity) {
        if (!StringUtils.isEmpty(entity.getCoverUrl()) && !entity.getCoverUrl().startsWith("https")) {
            String url = OssUtil.uploadImgByBase64("wolf2w/images", UUID.randomUUID() + ".jpg", entity.getCoverUrl());
            entity.setCoverUrl(url);
        }
        // 查询修改之前的数据
        final Strategy strategy = getById(entity.getId());
        // 2. 补充分类、主题、目的地的名称
        this.setThemeCatalogDestInfo(entity);
        // 3. 修改内容
        return Boolean.TRUE.equals(transactionTemplate.execute(status -> {
            boolean update = super.updateById(entity);
            // 3. 修改内容
            entity.getContent().setId(entity.getId());
            int row = strategyContentMapper.updateById(entity.getContent());
            boolean result = update && row > 0;
            if (result && !strategy.getDestId().equals(entity.getDestId())) {
                // 10. 发布新增攻略事件
                ctx.publishEvent(new StrategyEvent(OperateTypeEnum.UPDATE, strategy, entity));
            }
            if (result) {
                SpringContextUtil.publishEvent(new StrategyDataSyncEvent(OperateTypeEnum.UPDATE, entity));
            }
            return result;
        }));
    }

    private void setThemeCatalogDestInfo(Strategy entity) {
        long totalBegin = System.currentTimeMillis();
        if (entity.getThemeId() != null) {
            long begin = System.currentTimeMillis();
            StrategyTheme theme = strategyThemeService.getById(entity.getThemeId());
            if (theme != null) {
                entity.setThemeName(theme.getName());
            }
            System.out.println("主题耗时：" + (System.currentTimeMillis() - begin));
        }
        if (entity.getCatalogId() != null) {
            long begin = System.currentTimeMillis();
            StrategyCatalog catalog = strategyCatalogService.getById(entity.getCatalogId());
            if (catalog != null) {
                entity.setCatalogName(catalog.getName());
                entity.setDestId(catalog.getDestId());
                entity.setDestName(catalog.getDestName());
            }
            System.out.println("分类耗时：" + (System.currentTimeMillis() - begin));
        }
        long begin = System.currentTimeMillis();
        // 4. 判断当前目的地是否是国外
        List<Destination> toasts = destinationService.queryToasts(entity.getDestId());
        if (!CollectionUtils.isEmpty(toasts)) {
            Destination contry = toasts.get(0);
            entity.setIsabroad(contry.getId() == 1L ? Strategy.ABROAD_NO : Strategy.ABROAD_YES);
        }
        System.out.println("是否国外耗时：" + (System.currentTimeMillis() - begin));
        System.out.println("查询额外数据总耗时：" + (System.currentTimeMillis() - totalBegin));
    }

    private void overrideProp(Strategy entity) {
        entity.setState(Strategy.STATE_NORMAL);
        entity.setViewnum(0);
        entity.setFavornum(0);
        entity.setSharenum(0);
        entity.setThumbsupnum(0);
        entity.setReplynum(0);
    }
}

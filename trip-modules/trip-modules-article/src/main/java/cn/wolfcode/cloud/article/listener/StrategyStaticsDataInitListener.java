package cn.wolfcode.cloud.article.listener;

import cn.wolfcode.cloud.article.domain.Strategy;
import cn.wolfcode.cloud.article.service.StrategyService;
import cn.wolfcode.cloud.core.util.DateUtils;
import cn.wolfcode.cloud.redis.key.RedisKeyEnum;
import cn.wolfcode.cloud.redis.service.RedisService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 攻略统计数据初始化
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class StrategyStaticsDataInitListener implements ApplicationListener<ContextRefreshedEvent> {

    private final StrategyService strategyService;
    private final RedisService redisService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        // 集群环境下，由于多个服务实例的运行，每个服务实例都会创建对应的容器
        // 也就导致每个容器，都会有容器启动时期，此时都会触发容器刷新事件，因此每个容器中都会执行一段数据统计代码
        ApplicationContext context = event.getApplicationContext();
        if (context instanceof AnnotationConfigServletWebServerApplicationContext) {
            // 1. 利用 redis 原子性命令，设置一个执行中的标识到 redis，设置过期时间为今天最后一秒
            RedisKeyEnum prefix = RedisKeyEnum.STRATEGY_STATICS_DATA_INIT_FLAG;
            // 获取到当前距离今天最后一秒剩余的秒数
            prefix.setTimeout(DateUtils.getBetweenSecondsOfNowToLastSeconds());
            boolean notExists = redisService.setnx(prefix, "1");
            // 2. 判断是否有这个标识，如果没有才执行，否则不执行
            if (!notExists) {
                log.info("[攻略统计数据] 今日已完成攻略初始化，不再执行....");
                return;
            }
            log.info("[攻略统计数据] 准备开始执行攻略统计数据初始化.....");
            // 利用 redis 的 keys 命令，以模糊匹配的形式查询所有 key
            RedisKeyEnum strategyStaticsHash = RedisKeyEnum.STRATEGY_STATICS_HASH;
            Set<String> keys = redisService.keys(strategyStaticsHash.concat("*"));
            // 将获取到的所有 key 转换为 id 集合
            List<Long> idList = keys.stream()
                    .map(key -> {
                        try {
                            String keyPrefix = strategyStaticsHash.getPrefix() + ":";
                            String idStr = key.substring(keyPrefix.length());
                            return Long.parseLong(idStr);
                        } catch (Exception ignore) {
                        }
                        return null;
                    }).collect(Collectors.toList());

            LambdaQueryWrapper<Strategy> wrapper = new LambdaQueryWrapper<>();
            // Spring Batch
            int total = 0, current = 1, size = 7;
            do {
                int count = 0;
                int start = (current - 1) * size;
                wrapper.clear();
                // 1. 查询所有攻略数据
                wrapper.notIn(!CollectionUtils.isEmpty(idList), Strategy::getId, idList);
                wrapper.last("limit " + start + ", " + size);
                List<Strategy> strategies = strategyService.list(wrapper);
                if (strategies.size() == 0) {
                    log.info("[攻略统计数据] 统计数据完成，当前第{}页，共初始化{}数据...", current, total);
                    return;
                }
                // 2. 遍历攻略数据，判断当前攻略在 redis 中是否存在
                for (Strategy strategy : strategies) {
                    // 3. 如果不存在，将攻略对象转换为 map
                    boolean exists = redisService.exists(RedisKeyEnum.STRATEGY_STATICS_HASH, strategy.getId() + "");
                    if (!exists) {
                        // 4. 将 map 存入 redis
                        Map<String, Object> map = strategy.toStaticsMap();
                        redisService.hputAll(RedisKeyEnum.STRATEGY_STATICS_HASH, map, strategy.getId() + "");
                        count++; // 计算每页统计了多少
                        total++; // 计算总数
                    }
                }
                log.info("[攻略统计数据] 查询第{}页数据，查询到{}条数据，共初始化{}条数据...", current, strategies.size(), count);
                current++;
            } while (true);
        }
    }

    public static void main(String[] args) {
        String prefix = "articles:strategies:statics:";
        String key = prefix + 12;

        String id = key.substring(prefix.length());
        System.out.println("id = " + id);
    }
}

package cn.wolfcode.cloud.article.service.impl;

import cn.wolfcode.cloud.article.vo.StrategyCatalogGroup;
import cn.wolfcode.cloud.article.domain.StrategyCatalog;
import cn.wolfcode.cloud.article.mapper.StrategyCatalogMapper;
import cn.wolfcode.cloud.article.service.StrategyCatalogService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class StrategyCatalogServiceImpl extends ServiceImpl<StrategyCatalogMapper, StrategyCatalog> implements StrategyCatalogService {
    @Override
    public List<StrategyCatalogGroup> findGroupList() {
        // 1. 查询所有分类
        List<StrategyCatalog> catalogs = list();
        // 2. 转换为 group list
        Map<String, List<StrategyCatalog>> map = catalogs.stream()
                .collect(Collectors.groupingBy(StrategyCatalog::getDestName));

        return map.entrySet().stream()
                .map(entry -> new StrategyCatalogGroup(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public List<StrategyCatalog> findByDestId(Long destId) {
        return list(new LambdaQueryWrapper<StrategyCatalog>().eq(StrategyCatalog::getDestId, destId));
    }
}

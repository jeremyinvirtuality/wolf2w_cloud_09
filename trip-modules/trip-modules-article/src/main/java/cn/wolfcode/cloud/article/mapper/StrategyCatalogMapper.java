package cn.wolfcode.cloud.article.mapper;

import cn.wolfcode.cloud.article.domain.StrategyCatalog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface StrategyCatalogMapper extends BaseMapper<StrategyCatalog> {
}

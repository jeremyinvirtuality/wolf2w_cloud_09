package cn.wolfcode.cloud.article.controller;

import cn.wolfcode.cloud.article.domain.Banner;
import cn.wolfcode.cloud.article.service.BannerService;
import cn.wolfcode.cloud.core.util.R;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api("轮播图管理")
@RestController
@RequestMapping("/banners")
@RequiredArgsConstructor
public class BannerController {

    private final BannerService bannerService;

    @ApiOperation("游记轮播图")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "名称", defaultValue = "xiaoliu"),
            @ApiImplicitParam(name = "age", value = "年龄", defaultValue = "18")
    })
    @GetMapping("/travels")
    public R<List<Banner>> travels(String name, String age) {
        return R.ok(bannerService.findByType(Banner.TYPE_TRAVEL));
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "请求成功"),
            @ApiResponse(code = 400, message = "参数错误"),
            @ApiResponse(code = 401, message = "未认证"),
            @ApiResponse(code = 403, message = "未授权")
    })
    @ApiOperation("攻略轮播图")
    @GetMapping("/strategies")
    public R<List<Banner>> strategies() {
        return R.ok(bannerService.findByType(Banner.TYPE_STRATEGY));
    }
}

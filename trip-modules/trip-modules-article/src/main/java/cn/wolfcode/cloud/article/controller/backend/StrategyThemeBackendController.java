package cn.wolfcode.cloud.article.controller.backend;

import cn.wolfcode.cloud.article.domain.StrategyTheme;
import cn.wolfcode.cloud.article.service.StrategyThemeService;
import cn.wolfcode.cloud.core.query.QueryObject;
import cn.wolfcode.cloud.core.util.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/backend/strategies/themes")
@RequiredArgsConstructor
public class StrategyThemeBackendController {

    private final StrategyThemeService strategyThemeService;

    @GetMapping("/list")
    public R<Page<StrategyTheme>> list(QueryObject qo) {
        return R.ok(strategyThemeService.page(new Page<>(qo.getCurrent(), qo.getSize())));
    }

    @GetMapping("/detail")
    public R<StrategyTheme> detailById(Long id) {
        return R.ok(strategyThemeService.getById(id));
    }

    @PostMapping("/save")
    public R<?> save(StrategyTheme strategyTheme) {
        strategyThemeService.save(strategyTheme);
        return R.ok();
    }

    @PostMapping("/updateById")
    public R<?> updateById(StrategyTheme strategyTheme) {
        strategyThemeService.updateById(strategyTheme);
        return R.ok();
    }

    @PostMapping("/deleteById")
    public R<?> deleteById(Long id) {
        strategyThemeService.removeById(id);
        return R.ok();
    }
}

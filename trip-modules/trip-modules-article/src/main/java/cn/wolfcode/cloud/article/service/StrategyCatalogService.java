package cn.wolfcode.cloud.article.service;

import cn.wolfcode.cloud.article.vo.StrategyCatalogGroup;
import cn.wolfcode.cloud.article.domain.StrategyCatalog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface StrategyCatalogService extends IService<StrategyCatalog> {

    List<StrategyCatalogGroup> findGroupList();

    List<StrategyCatalog> findByDestId(Long destId);
}

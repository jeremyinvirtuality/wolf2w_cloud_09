package cn.wolfcode.cloud.article.mapper;

import cn.wolfcode.cloud.article.domain.Destination;
import cn.wolfcode.cloud.article.vo.StrategyCondition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

public interface DestinationMapper extends BaseMapper<Destination> {
    List<StrategyCondition> selectConditionsByAbroad(int abroad);
}

package cn.wolfcode.cloud.article.controller.backend;

import cn.wolfcode.cloud.article.vo.StrategyCatalogGroup;
import cn.wolfcode.cloud.article.domain.StrategyCatalog;
import cn.wolfcode.cloud.article.service.StrategyCatalogService;
import cn.wolfcode.cloud.core.util.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/backend/strategies/catalogs")
@RequiredArgsConstructor
public class StrategyCatalogBackendController {

    private final StrategyCatalogService strategyCatalogService;

    @GetMapping("/list")
    public R<Page<StrategyCatalog>> list(long current, long size) {
        return R.ok(strategyCatalogService.page(new Page<>(current, size)));
    }

    @GetMapping("/detail")
    public R<StrategyCatalog> detailById(Long id) {
        return R.ok(strategyCatalogService.getById(id));
    }

    @GetMapping("/groups")
    public R<List<StrategyCatalogGroup>> groupList() {
        return R.ok(strategyCatalogService.findGroupList());
    }

    @PostMapping("/save")
    public R<?> save(StrategyCatalog strategyCatalog) {
        strategyCatalogService.save(strategyCatalog);
        return R.ok();
    }

    @PostMapping("/updateById")
    public R<?> updateById(StrategyCatalog strategyCatalog) {
        strategyCatalogService.updateById(strategyCatalog);
        return R.ok();
    }

    @PostMapping("/deleteById")
    public R<?> deleteById(Long id) {
        strategyCatalogService.removeById(id);
        return R.ok();
    }
}

package cn.wolfcode.cloud.article.controller;

import cn.wolfcode.cloud.article.domain.Region;
import cn.wolfcode.cloud.article.service.RegionService;
import cn.wolfcode.cloud.core.util.R;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/regions")
@RequiredArgsConstructor
public class RegionController {

    private final RegionService regionService;

    @GetMapping("/hotList")
    public R<List<Region>> hotList() {
        List<Region> list = regionService.findHotList();
        return R.ok(list);
    }
}

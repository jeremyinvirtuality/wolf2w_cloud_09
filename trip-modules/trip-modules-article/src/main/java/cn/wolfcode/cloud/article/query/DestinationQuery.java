package cn.wolfcode.cloud.article.query;

import cn.wolfcode.cloud.core.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DestinationQuery extends QueryObject {

    private Long parentId;
}

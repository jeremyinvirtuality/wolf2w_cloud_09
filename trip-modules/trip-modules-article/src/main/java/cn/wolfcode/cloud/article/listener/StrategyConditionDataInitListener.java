package cn.wolfcode.cloud.article.listener;

import cn.wolfcode.cloud.article.service.StrategyConditionService;
import cn.wolfcode.cloud.article.vo.StrategyCondition;
import cn.wolfcode.cloud.redis.key.RedisKeyEnum;
import cn.wolfcode.cloud.redis.service.RedisService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.wolfcode.cloud.article.vo.StrategyCondition.*;

/**
 * 攻略条件数据初始化监听器
 * 在 Spring 容器启动的时候运行
 * 去查询 mysql 中存在，redis 中不存在的数据，将其存入 redis
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class StrategyConditionDataInitListener implements ApplicationListener<ContextRefreshedEvent> {

    private final StrategyConditionService strategyConditionService;
    private final RedisService redisService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        ApplicationContext context = event.getApplicationContext();
        if (context instanceof AnnotationConfigServletWebServerApplicationContext) {
            log.info("[攻略条件初始化] 准备开始查询攻略条件数据.....");
            int[] types = new int[]{StrategyCondition.TYPE_ABROAD, StrategyCondition.TYPE_CHINA, StrategyCondition.TYPE_THEME};
            int count = 0;
            for (int type : types) {
                List<StrategyCondition> conditions = null;
                if (type != StrategyCondition.TYPE_THEME) {
                    conditions = strategyConditionService.findByDestType(type);
                } else {
                    conditions = strategyConditionService.findByThemes();
                }

                // 遍历查询到条件
                for (StrategyCondition condition : conditions) {
                    // 获取条件的类型，判断是否是主题
                    boolean exists = false;
                    if (condition.getType() == TYPE_THEME) {
                        // 存入主题 hash
                        exists = putConditionCount(RedisKeyEnum.STRATEGY_CONDITIONS_THEMES, condition);
                    } else {
                        // 存入目的地 hash
                        exists = putConditionCount(RedisKeyEnum.STRATEGY_CONDITIONS_DESTINATIONS, condition);
                    }
                    if (!exists) {
                        count++;
                    }
                }
            }
            log.info("[攻略条件初始化] 本次初始化条件完成，共计初始化：{}条数据...", count);
        }
    }

    private boolean putConditionCount(RedisKeyEnum prefix, StrategyCondition condition) {
        // 查询 redis 中是否有当前条件
        String field = ID_KEY + condition.getId();
        boolean ret = redisService.hexists(prefix, field);
        if (!ret) {
            // 如果不存在，就将当前数据存入 redis
            Map<String, Object> map = new HashMap<>();
            map.put(field, condition.getId());
            map.put(NAME_KEY + condition.getId(), condition.getName());
            map.put(COUNT_KEY + condition.getId(), condition.getCount());
            map.put(TYPE_KEY + condition.getId(), condition.getType());
            redisService.hputAll(prefix, map);
        }
        return ret;
    }
}

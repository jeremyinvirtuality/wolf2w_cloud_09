package cn.wolfcode.cloud.article.controller;

import cn.wolfcode.cloud.article.domain.Destination;
import cn.wolfcode.cloud.article.service.DestinationService;
import cn.wolfcode.cloud.core.util.R;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/destinations")
@RequiredArgsConstructor
public class DestinationController {

    private final DestinationService destinationService;

    @GetMapping("/hotList")
    public R<List<Destination>> hotList(Long rid) {
        List<Destination> list = destinationService.listByRid(rid);
        return R.ok(list);
    }

    @GetMapping("/toasts")
    public R<List<Destination>> queryToasts(Long destId) {
        return R.ok(destinationService.queryToasts(destId));
    }
}

package cn.wolfcode.cloud.article.controller.backend;

import cn.wolfcode.cloud.article.domain.Destination;
import cn.wolfcode.cloud.article.domain.Region;
import cn.wolfcode.cloud.article.service.DestinationService;
import cn.wolfcode.cloud.article.service.RegionService;
import cn.wolfcode.cloud.core.util.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/backend/regions")
@RequiredArgsConstructor
public class RegionBackendController {

    private final RegionService regionService;
    private final DestinationService destinationService;

    @GetMapping("/list")
    public R<Page<Region>> list(long current, long size) {
        return R.ok(regionService.page(new Page<>(current, size)));
    }

    @GetMapping("/detail")
    public R<Region> detailById(Long id) {
        return R.ok(regionService.getById(id));
    }

    @GetMapping("/{id}/destinations")
    public R<List<Destination>> getDestinationsByRegionId(@PathVariable Long id) {
        Region region = regionService.getById(id);
        List<Destination> list = destinationService.listByIds(region.parseRefIds());
        return R.ok(list);
    }

    @PostMapping("/save")
    public R<?> save(Region region) {
        regionService.save(region);
        return R.ok();
    }

    @PostMapping("/updateById")
    public R<?> updateById(Region region) {
        regionService.updateById(region);
        return R.ok();
    }

    @PostMapping("/deleteById")
    public R<?> deleteById(Long id) {
        regionService.removeById(id);
        return R.ok();
    }
}

package cn.wolfcode.cloud.article.controller.backend;

import cn.wolfcode.cloud.article.service.StaticsService;
import cn.wolfcode.cloud.core.util.R;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/backend/statics/ranking")
@RequiredArgsConstructor
public class StatRankController {

    private final StaticsService staticsService;

    @PostMapping("/{type}")
    public R<?> rankingByType(@PathVariable Integer type) {
        staticsService.rankingByType(type);
        return R.ok();
    }
}

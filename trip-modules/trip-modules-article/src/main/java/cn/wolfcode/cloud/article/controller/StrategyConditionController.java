package cn.wolfcode.cloud.article.controller;

import cn.wolfcode.cloud.article.service.StrategyConditionService;
import cn.wolfcode.cloud.core.util.R;
import com.alibaba.fastjson2.JSONObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/strategies/conditions")
@RequiredArgsConstructor
public class StrategyConditionController {

    private final StrategyConditionService strategyConditionService;

    @GetMapping
    public R<JSONObject> conditions() {
        JSONObject json = strategyConditionService.findFromRedis();
        return R.ok(json);
    }
}

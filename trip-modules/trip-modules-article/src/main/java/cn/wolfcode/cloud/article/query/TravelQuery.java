package cn.wolfcode.cloud.article.query;

import cn.wolfcode.cloud.article.domain.Travel;
import cn.wolfcode.cloud.article.vo.RangeParam;
import cn.wolfcode.cloud.core.query.QueryObject;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class TravelQuery extends QueryObject {

    private static final String DAY_TYPE = "dayType:";
    private static final String TRAVEL_TIME_TYPE = "travelTimeType:";
    private static final String CONSUMER_TYPE = "consumeType:";

    private static final Map<String, RangeParam> TYPE_MAP = new HashMap<>();

    static {
        // 出行天数数据初始化
        TYPE_MAP.put(DAY_TYPE + 1, new RangeParam(0, 3));
        TYPE_MAP.put(DAY_TYPE + 2, new RangeParam(4, 7));
        TYPE_MAP.put(DAY_TYPE + 3, new RangeParam(8, 14));
        TYPE_MAP.put(DAY_TYPE + 4, new RangeParam(15, 99999));

        // 人均花费数据初始化
        TYPE_MAP.put(CONSUMER_TYPE + 1, new RangeParam(1, 999));
        TYPE_MAP.put(CONSUMER_TYPE + 2, new RangeParam(1000, 6000));
        TYPE_MAP.put(CONSUMER_TYPE + 3, new RangeParam(6000, 20000));
        TYPE_MAP.put(CONSUMER_TYPE + 4, new RangeParam(20001, 99999999));

        // 出发时间数据初始化
        TYPE_MAP.put(TRAVEL_TIME_TYPE + 1, new RangeParam(1, 2));
        TYPE_MAP.put(TRAVEL_TIME_TYPE + 2, new RangeParam(3, 4));
        TYPE_MAP.put(TRAVEL_TIME_TYPE + 3, new RangeParam(5, 6));
        TYPE_MAP.put(TRAVEL_TIME_TYPE + 4, new RangeParam(7, 8));
        TYPE_MAP.put(TRAVEL_TIME_TYPE + 5, new RangeParam(9, 10));
        TYPE_MAP.put(TRAVEL_TIME_TYPE + 6, new RangeParam(11, 12));
    }

    private Long destId;
    private SFunction<Travel, ?> orderBy;
    private RangeParam dayType;
    private RangeParam travelTimeType;
    private RangeParam consumeType;

    public void setDayType(Integer dayType) {
        this.dayType = TYPE_MAP.get(DAY_TYPE + dayType);
    }

    public void setTravelTimeType(Integer travelTimeType) {
        this.travelTimeType = TYPE_MAP.get(TRAVEL_TIME_TYPE + travelTimeType);
    }

    public void setConsumeType(Integer consumeType) {
        this.consumeType = TYPE_MAP.get(CONSUMER_TYPE + consumeType);
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = "viewnum".equals(orderBy) ? Travel::getViewnum : Travel::getCreateTime;
    }
}

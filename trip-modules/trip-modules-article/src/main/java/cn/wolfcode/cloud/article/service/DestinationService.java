package cn.wolfcode.cloud.article.service;

import cn.wolfcode.cloud.article.domain.Destination;
import cn.wolfcode.cloud.article.query.DestinationQuery;
import cn.wolfcode.cloud.article.vo.StrategyCondition;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface DestinationService extends IService<Destination> {
    Page<Destination> queryPage(DestinationQuery qo);

    List<Destination> queryToasts(Long destId);

    List<Destination> listByRid(Long rid);

    List<StrategyCondition> findConditionsByAbroad(int abroad);
}

package cn.wolfcode.cloud.article;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class TripArticleServer {

    public static void main(String[] args) {
        SpringApplication.run(TripArticleServer.class, args);
    }
}

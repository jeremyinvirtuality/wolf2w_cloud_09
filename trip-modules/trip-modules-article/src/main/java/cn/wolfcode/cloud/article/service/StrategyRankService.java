package cn.wolfcode.cloud.article.service;

import cn.wolfcode.cloud.article.domain.StrategyRank;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface StrategyRankService extends IService<StrategyRank> {
    List<StrategyRank> findByType(int type);
}

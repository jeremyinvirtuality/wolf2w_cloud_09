package cn.wolfcode.cloud.article.mapper;

import cn.wolfcode.cloud.article.domain.Travel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TravelMapper extends BaseMapper<Travel> {
}

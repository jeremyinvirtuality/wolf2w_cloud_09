package cn.wolfcode.cloud.article.listener;

import cn.wolfcode.cloud.article.domain.Destination;
import cn.wolfcode.cloud.article.domain.Strategy;
import cn.wolfcode.cloud.article.enums.OperateTypeEnum;
import cn.wolfcode.cloud.article.events.StrategyEvent;
import cn.wolfcode.cloud.article.service.DestinationService;
import cn.wolfcode.cloud.redis.key.RedisKeyEnum;
import cn.wolfcode.cloud.redis.service.RedisService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static cn.wolfcode.cloud.article.vo.StrategyCondition.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class StrategyConditionCountListener implements ApplicationListener<StrategyEvent> {

    private final RedisService redisService;
    private final DestinationService destinationService;

    @Override
    public void onApplicationEvent(StrategyEvent event) {
        log.info("[攻略条件统计监听器] 收到攻略操作事件：type={}, source={}", event.getType(), event.getSource());
        RedisKeyEnum destinationsPrefix = RedisKeyEnum.STRATEGY_CONDITIONS_DESTINATIONS;
        RedisKeyEnum themesPrefix = RedisKeyEnum.STRATEGY_CONDITIONS_THEMES;
        String field = COUNT_KEY;
        Strategy strategy = (Strategy) event.getSource();
        // 根据攻略的目的地 id 查询目的地对象，得到父目的地id与名称
        Destination destination = destinationService.getById(strategy.getDestId());
        if (destination == null) {
            log.warn("[攻略条件统计监听器] 查询攻略所属目的地数据异常：{}", strategy.getDestId());
            return;
        }
        // 获取目的地 id
        Long destId = handleDestId(strategy, destination);

        if (OperateTypeEnum.INSERT.equals(event.getType())) {
            // 处理新增逻辑
            this.handleInsert(destinationsPrefix, themesPrefix, field, strategy, destination, destId);
        } else if (OperateTypeEnum.DELETE.equals(event.getType())) {
            // 删除逻辑处理
            this.handleDelete(destinationsPrefix, themesPrefix, field, strategy, destId);
        } else {
            // 处理修改逻辑
            // 对新的目的地数量 +1（做新增操作）
            this.handleInsert(destinationsPrefix, themesPrefix, field, strategy, destination, destId);

            // 将攻略对象赋值为旧的攻略对象
            strategy = event.getOriginal();
            // 基于旧的目的地 id 重新查询目的地对象
            destination = destinationService.getById(strategy.getDestId());
            if (destination == null) {
                log.warn("[攻略条件统计监听器] 查询攻略所属目的地数据异常：{}", strategy.getDestId());
                return;
            }
            // 重新处理目的地 id
            destId = handleDestId(strategy, destination);
            // 对旧的目的地数量 -1（做删除操作）
            this.handleDelete(destinationsPrefix, themesPrefix, field, strategy, destId);
        }
    }

    private static Long handleDestId(Strategy strategy, Destination destination) {
        Long destId = null;
        if (Strategy.ABROAD_NO == strategy.getIsabroad()) {
            // 国内：父目的地 id == 1 ? 返回自己的 id ： 父目的地id
            destId = destination.getParentId() == 1 ? destination.getId() : destination.getParentId();
        } else {
            // 国外：父目的地 id 为 null ？ 返回自己的 id ： 父目的地id
            destId = destination.getParentId() == null ? destination.getId() : destination.getParentId();
        }
        return destId;
    }

    private void handleInsert(RedisKeyEnum destinationsPrefix, RedisKeyEnum themesPrefix, String field, Strategy strategy, Destination destination, Long destId) {
        // 新增攻略
        String destField = field + destId;
        // 目的地、主题
        // 执行 redis 增加数量操作
        Long count = redisService.hincrement(destinationsPrefix, destField, 1);
        if (count == 1L) {
            // 如果不存在，就将当前数据存入 redis
            Map<String, Object> map = new HashMap<>();
            map.put(ID_KEY + destId, destId);
            String name = null;
            name = destination.getParentName();
            if (StringUtils.isEmpty(destination.getParentName()) || destination.getParentId() == 1L) {
                name = destination.getName();
            }
            map.put(NAME_KEY + destId, name);
            Integer type = strategy.getIsabroad() == Strategy.ABROAD_YES ? TYPE_ABROAD : TYPE_CHINA;
            map.put(TYPE_KEY + destId, type);
            redisService.hputAll(destinationsPrefix, map);
        }
        String themeField = field + strategy.getThemeId();
        // 主题增加
        count = redisService.hincrement(themesPrefix, themeField, 1);
        if (count == 1L) {
            // 说明之前没有主题数据，需要新增
            Map<String, Object> map = new HashMap<>();
            map.put(ID_KEY + strategy.getThemeId(), strategy.getThemeId());
            map.put(NAME_KEY + strategy.getThemeId(), strategy.getThemeName());
            map.put(TYPE_KEY + strategy.getThemeId(), TYPE_THEME);
            redisService.hputAll(themesPrefix, map);
        }
    }

    private void handleDelete(RedisKeyEnum destinationsPrefix, RedisKeyEnum themesPrefix, String field, Strategy strategy, Long destId) {
        Long themeId = strategy.getThemeId();
        // 减目的地条件统计数据
        subConditionCount(destinationsPrefix, field, destId);
        subConditionCount(themesPrefix, field, themeId);
    }

    private void subConditionCount(RedisKeyEnum prefix, String field, Long id) {
        // 删除攻略
        field += id;
        // 目的地数量-1
        Long count = redisService.hincrement(prefix, field, -1);
        if (count == 0) {
            Object[] arr = {COUNT_KEY + id, ID_KEY + id, NAME_KEY + id, TYPE_KEY + id};
            redisService.hdel(prefix, arr); // count:xxx, id:xxx, name:xxx, type:xxx
        }
    }
}

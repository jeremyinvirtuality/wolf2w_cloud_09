package cn.wolfcode.cloud.article.service.impl;

import cn.wolfcode.cloud.article.domain.Banner;
import cn.wolfcode.cloud.article.mapper.BannerMapper;
import cn.wolfcode.cloud.article.service.BannerService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BannerServiceImpl extends ServiceImpl<BannerMapper, Banner> implements BannerService {
    @Override
    public List<Banner> findByType(Integer type) {
        LambdaQueryWrapper<Banner> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Banner::getState, Banner.STATE_NORMAL)
                .eq(Banner::getType, type)
                .orderByAsc(Banner::getSeq);

        return list(wrapper);
    }
}

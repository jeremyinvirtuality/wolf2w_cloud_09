package cn.wolfcode.cloud.article.service;

import cn.wolfcode.cloud.article.vo.StrategyCondition;
import com.alibaba.fastjson2.JSONObject;

import java.util.List;

public interface StrategyConditionService {

    List<StrategyCondition> findByDestType(int type);

    List<StrategyCondition> findByThemes();

    JSONObject findFromRedis();
}

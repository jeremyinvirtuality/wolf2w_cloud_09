package cn.wolfcode.cloud.article.service.impl;

import cn.wolfcode.cloud.article.domain.Destination;
import cn.wolfcode.cloud.article.domain.Region;
import cn.wolfcode.cloud.article.mapper.DestinationMapper;
import cn.wolfcode.cloud.article.query.DestinationQuery;
import cn.wolfcode.cloud.article.service.DestinationService;
import cn.wolfcode.cloud.article.service.RegionService;
import cn.wolfcode.cloud.article.vo.StrategyCondition;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DestinationServiceImpl extends ServiceImpl<DestinationMapper, Destination> implements DestinationService {

    private final RegionService regionService;

    @Override
    public Page<Destination> queryPage(DestinationQuery qo) {
        LambdaQueryWrapper<Destination> wrapper = new LambdaQueryWrapper<>();
        // 如果 parentId 没有值，就查询 parent_id is null 的数据
        wrapper.isNull(qo.getParentId() == null, Destination::getParentId);
        // 如果 parentId 有值，查询 parent_id = #{parentId} 的数据
        wrapper.eq(qo.getParentId() != null, Destination::getParentId, qo.getParentId());

        return page(new Page<>(qo.getCurrent(), qo.getSize()), wrapper);
    }

    @Override
    public List<Destination> queryToasts(Long destId) {
        List<Destination> list = new ArrayList<>();
        do {
            // 1. 先基于当前 destId 查询对象
            Destination dest = getById(destId);
            if (dest == null)
                break;

            // 2. 只要当前对象不为 null，就存入要返回的集合中
            list.add(dest);
            destId = dest.getParentId();
            // 3. 判断当前对象的 parentId 不为 null，就继续将 parentId 作为 destId 继续第一步重复查询
        } while (true);

        // 4. 反转 list 集合
        Collections.reverse(list);
        return list;
    }

    @Override
    public List<Destination> listByRid(Long rid) {
        List<Destination> destinations = null;
        if (rid == -1) {
            // 查询国内
            destinations = list(new LambdaQueryWrapper<Destination>().eq(Destination::getParentId, 1));
        } else {
            Region region = regionService.getById(rid);
            List<Long> destIds = region.parseRefIds();
            // 查询其他地区
            destinations = listByIds(destIds);
        }
        // 查询省份下的所有城市
        for (Destination parent : destinations) {
            List<Destination> children =
                    list(new LambdaQueryWrapper<Destination>()
                            .eq(Destination::getParentId, parent.getId())
                            .last("limit 10")
                    );
            parent.setChildren(children);
        }
        return destinations;
    }

    @Override
    public List<StrategyCondition> findConditionsByAbroad(int abroad) {
        return baseMapper.selectConditionsByAbroad(abroad);
    }
}

package cn.wolfcode.cloud.article.controller;

import cn.wolfcode.cloud.article.domain.StrategyRank;
import cn.wolfcode.cloud.article.service.StrategyRankService;
import cn.wolfcode.cloud.core.util.R;
import com.alibaba.fastjson2.JSONObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/strategies/ranks")
@RequiredArgsConstructor
public class StrategyRankController {

    private final StrategyRankService strategyRankService;

    @GetMapping
    public R<JSONObject> ranks() {
        List<StrategyRank> abroadRank = strategyRankService.findByType(StrategyRank.TYPE_ABROAD);
        List<StrategyRank> chinaRank = strategyRankService.findByType(StrategyRank.TYPE_CHINA);
        List<StrategyRank> hotRank = strategyRankService.findByType(StrategyRank.TYPE_HOT);

        return R.ok(JSONObject.of("abroadRank", abroadRank, "chinaRank", chinaRank, "hotRank", hotRank));
    }
}

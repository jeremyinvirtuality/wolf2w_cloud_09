package cn.wolfcode.cloud.article.service;

import cn.wolfcode.cloud.article.domain.Destination;
import cn.wolfcode.cloud.article.domain.Strategy;
import cn.wolfcode.cloud.article.domain.Travel;
import cn.wolfcode.cloud.article.feign.SearchFeignService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@SpringBootTest
public class ElasticsearchDataSyncEsTest {

    @Autowired
    private StrategyService strategyService;
    @Autowired
    private TravelService travelService;
    @Autowired
    private DestinationService destinationService;
    @Autowired
    private SearchFeignService searchFeignService;

    @Test
    public void testBatchSaveStrategy() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("batch-save");
        List<Strategy> strategies = strategyService.list();
        final AtomicInteger count = new AtomicInteger(0);

        List<Strategy> batchSave = new ArrayList<>(1000);
        for (int i = 0; i < strategies.size(); i++) {
            batchSave.add(strategies.get(i));
            int current = count.incrementAndGet();
            if (current % 1000 == 0 || i == strategies.size() - 1) {
                // 保存一次
                searchFeignService.batchSaveStrategy(batchSave);
                batchSave.clear();
            }
        }
        stopWatch.stop();
        System.out.println("任务执行时间 = " + stopWatch.getTotalTimeSeconds());
    }

    @Test
    public void testBatchSaveDestination() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("batch-save");
        List<Destination> destinations = destinationService.list();
        final AtomicInteger count = new AtomicInteger(0);

        List<Destination> batchSave = new ArrayList<>(1000);
        for (int i = 0; i < destinations.size(); i++) {
            batchSave.add(destinations.get(i));
            int current = count.incrementAndGet();
            if (current % 1000 == 0 || i == destinations.size() - 1) {
                // 保存一次
                searchFeignService.batchSaveDestination(batchSave);
                batchSave.clear();
            }
        }
        stopWatch.stop();
        System.out.println("任务执行时间 = " + stopWatch.getTotalTimeSeconds());
    }

    @Test
    public void testBatchSaveTravel() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("batch-save");
        List<Travel> travels = travelService.list();
        final AtomicInteger count = new AtomicInteger(0);

        List<Travel> batchSave = new ArrayList<>(1000);
        for (int i = 0; i < travels.size(); i++) {
            batchSave.add(travels.get(i));
            int current = count.incrementAndGet();
            if (current % 1000 == 0 || i == travels.size() - 1) {
                // 保存一次
                searchFeignService.batchSaveTravel(batchSave);
                batchSave.clear();
            }
        }
        stopWatch.stop();
        System.out.println("任务执行时间 = " + stopWatch.getTotalTimeSeconds());
    }
}

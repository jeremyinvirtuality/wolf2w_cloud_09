package cn.wolfcode.cloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TripGatewayServer {
    public static void main(String[] args) {
        SpringApplication.run(TripGatewayServer.class, args);
    }
}

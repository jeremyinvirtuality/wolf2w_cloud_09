var vue = new Vue({
    el:"#app",
    data:{
        param:{},
        page:{},
        themes:[],
        toasts:[]
    },
    methods:{
        themeSelect:function(themeId, event){
            $("._j_tag").removeClass("on")
            $(event.currentTarget).addClass("on");
            vue.doPage(1);
        },
        doPage:function(page){
            var param = getParams();
            var themeId = $("._j_tag.on").data("tid");
            ajaxGet("articles","/strategies/query",{themeId:themeId, destId:param.destId, current:page}, function (data) {
                vue.page = data.data;
                buildPage(vue.page.current, vue.page.pages,vue.doPage);
            })
        },
        queryToasts:function (){
            ajaxGet("articles","/destinations/toasts",{destId:this.param.destId}, function (data) {
                var list = data.data;
                vue.toasts = list;
            })
        },
        queryTheme:function (){
            ajaxGet("articles","/strategies/destinations/themes", {destId: this.param.destId}, function (data) {
                vue.themes = data.data;
            })

        }
    },
    mounted:function () {
        this.param = getParams();
        this.queryToasts();
        this.queryTheme();
        this.doPage(1);
    }
});

